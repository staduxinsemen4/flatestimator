import numpy
import torch
import torchvision.transforms as T


def classify_transforms(
        size=224,
        mean=(0.0, 0.0, 0.0),
        std=(1.0, 1.0, 1.0),
        interpolation: T.InterpolationMode = T.InterpolationMode.BILINEAR,
        crop_fraction: float = 1.,
):
    """
    Classification transforms for evaluation/inference. Inspired by timm/data/transforms_factory.py.

    Args:
        size (int): image size
        mean (tuple): mean values of RGB channels
        std (tuple): std values of RGB channels
        interpolation (T.InterpolationMode): interpolation mode. default is T.InterpolationMode.BILINEAR.
        crop_fraction (float): fraction of image to crop. default is 1.0.

    Returns:
        (T.Compose): torchvision transforms
    """
    print("used")
    tfl = [
        SquarePad(),
        T.Resize((256, 256)),
        T.ToTensor(),
        T.Normalize(
            mean=torch.tensor(mean),
            std=torch.tensor(std),
        ),
    ]

    return T.Compose(tfl)


import torchvision.transforms.functional as F


# class SquarePad:
#     def __call__(self, image):
#         s = image.size
#         max_wh = np.max([s[-1], s[-2]])
#         hp = int((max_wh - s[-1]) / 2)
#         vp = int((max_wh - s[-2]) / 2)
#         padding = (hp, vp, hp, vp)
#         return F.pad(image, padding, 0, 'constant')
class SquarePad:
    def __call__(self, image):

        img_sizes = image.size

        max_wh = max(img_sizes)
        p_left, p_top = [(max_wh - s) // 2 for s in img_sizes]
        p_right, p_bottom = [max_wh - (s + pad) for s, pad in zip(img_sizes, [p_left, p_top])]
        padding = (p_left, p_top, p_right, p_bottom)
        return F.pad(image, padding, 0, 'constant')
# image = Image.open("D:\\datasets\\aparts\\prepared_handed\\train\\2\\1ed3a3ad-3856205468_6.jpg")
# transforms = classify_transforms(size=256, crop_fraction=1)
# img = transforms(image)
#
# trans = torchvision.transforms.ToPILImage()
# tr_img = trans(img)
# print(tr_img.size)
# tr_img.show()

#
# dataset = ClassificationDataset
# img = torch.stack(
#     [self.transforms(Image.fromarray(cv2.cvtColor(im, cv2.COLOR_BGR2RGB))) for im in img], dim=0
# )
