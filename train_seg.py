from ultralytics import YOLO

if __name__ == '__main__':
    model_name = "yolov9c-seg.pt"
    dataset_name = "regions"

    model = YOLO(model_name)
    # model = YOLO("region/yolov8s_seg_region_2560_5000_batch3/weights/best.pt")
    #
    # model.train(data=f'D:\\datasets\\aparts\\prepared_handed', epochs=1000,
    #             batch=-1, pretrained=True, imgsz=256, project=dataset_name,
    #             name=f'{model_name}_{dataset_name}_handed_resize_ratio_scale_ext_1221_1000',
    #             resume=False, patience=1000, plots=True, shear=40, perspective=0.001, degrees=40, erasing=0.6, seed=200,
    #             )
    model.train(
        data='D:/datasets/region/region.yaml',
        imgsz=960,
        epochs=2500,
        patience=2500,
        batch=3,
        # workers=23,
        resume=False,
        single_cls=True,
        pretrained="yolov9c-seg.pt",
        # scale=1.,
        # shear=0.001,
        copy_paste=0.4,
        amp=False,
        device=0,
        plots=True, seed=200,
        # mask_ratio=1,
        name='yolov9c-seg.pt_region_960_192_amp_false',
        project="region")
    # freeze_support()
