import os

ROOT_DIR = os.path.dirname(__file__)

CHROME_PATH = "C:/Program Files/Google/Chrome/Application/chrome.exe"

CHROMEDRIVERS_PATH = os.path.join(ROOT_DIR, "utils/chromedrivers")

CHROMEDRIVER_DOWNLOAD_SRC = "https://chromedriver.chromium.org/downloads"

PHANTOMJS_PATH = os.path.join(ROOT_DIR, "utils/phantom/windows/phantomjs.exe")

DATASET_DOWNLOAD_SETTINGS_PATH = os.path.join(ROOT_DIR, "parsing_settings")

IMAGE_DOWNLOAD_DELAY = 1.2

ESTIMATOR_TASKS_PATH = os.path.join(ROOT_DIR, "estimator_tasks")

ESTIMATOR_MODEL_PATH = os.path.join(ROOT_DIR, "analytics/models/model_0015_0.946727")

ESTIMATOR_CLASSES_PATH = os.path.join(ROOT_DIR, "analytics/data/estimator_classes.txt")

ESTIMATOR_MODEL_SHAPE = (384, 384, 3)

ESTIMATOR_GPU_ENABLE = False
