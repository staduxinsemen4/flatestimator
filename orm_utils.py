from orm_models import User, db
from telegram import User as TelegramUser
from typing import Union


def get_updated_user(user: TelegramUser) -> User:
    db_user = get_user(user.id)
    args_match = dict(tg_user_id=user.id, first_name=user.first_name, last_name=user.last_name, is_bot=user.is_bot,
                      language_code=user.language_code, username=user.username)
    if db_user is None:
        db_user = User.create(**args_match)
    else:
        del args_match['tg_user_id']
        [setattr(db_user, key, val) for key, val in args_match.items()]
        db_user.save()
    return db_user


def get_user(user_id: int) -> Union[User, None]:
    return User.get_or_none(tg_user_id=user_id)
