from api_addresser import ApiAddresser
from api_config import api_url, email, password

api_addresser = ApiAddresser(api_url=api_url, email=email, password=password)
api_addresser.get_flats()