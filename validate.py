from cls_transforms import patch_train

patch_train()

from ultralytics import YOLO

if __name__ == "__main__":
    model = YOLO("model/yolov8s-cls.yaml_flats_quality_handed_resize_ratio_scale_ext_832_77_4/weights/best.pt")

    print(model.predict("photo_2024-05-11_17-20-17.jpg",show=True))
