from cls_transforms import patch_inference
from utils.region_recoverer import RegionRecoverer

patch_inference()

import traceback
from analytics import FlatEstimator
from callbacks.estimator_callbacks import EstimatorCallbacks
from callbacks.parser_callbacks import ParserCallbacks
from callbacks.database_telegram_callback import DataBaseTelegramCallback
from parsers import AvitoParser
from ultralytics import YOLO
from utils.captcha_solver import CaptchaSolver
import time
if __name__ == '__main__':

    # ultralytics.data.augment.classify_transforms = new_classify_transforms
    model = YOLO(
        "model/yolov8m-cls.pt_flats_quality_handed_resize_ratio_scale_ext_1221_1000/weights/best.pt")

    estimator = FlatEstimator(model)
    callback = DataBaseTelegramCallback(estimator, image_download_delay=.1)
    # callback = EstimatorCallbacks(estimator)
    # # # results = model("D:\\datasets\\aparts\\prepared\\flats_quality\\val\\1\\b383b36a-3986104881_5.jpg", show=True,
    # # #                 visualize=True)
    parser = AvitoParser(callback, headless=False, captcha_solver=CaptchaSolver(), region_recoverer=RegionRecoverer())
    time.sleep(3)
    try:
        # parser.webdriver.get("https://2ip.io/")
        # import time
        # time.sleep(20)
        # parser.parse_region("https://www.avito.ru/perm/kvartiry/1-k._kvartira_44_m_89_et._3783574841")
        # parser.parse_flat("https://www.avito.ru/perm/kvartiry/1-k._kvartira_408_m_1223_et._3078714124",123213)
        parser.parse_region(
            "https://www.avito.ru/perm/kvartiry/sdam/na_dlitelnyy_srok/do-60-tis-rubley-ASgBAgECAkSSA8gQ8AeQUgFFxpoMFXsiZnJvbSI6MCwidG8iOjYwMDAwfQ?context=H4sIAAAAAAAA_0q0MrSqLraysFJKK8rPDUhMT1WyLrYytlLKTSxQsq4FBAAA__8Xe4TEHwAAAA&drawId=7f1b0adb3e05440ebd3d732c42d69c1d&map=e30%3D",region_id=7)
    except Exception as e:
        print(traceback.format_exc())
    finally:
        parser.stop_parser()
    # parser.parse_flat("https://www.avito.ru/perm/kvartiry/kvartira-studiya_18_m_125_et._3747178074")
    # # parser.parse_flats_by_link("https://www.avito.ru/perm/kvartiry/sdam/na_dlitelnyy_srok-ASgBAgICAkSSA8gQ8AeQUg?drawId=a545167635bd386d6d3fa61f6fed21f5&map=eyJzZWFyY2hBcmVhIjp7ImxhdEJvdHRvbSI6NTcuOTkwMTc1MjEzMjY4MDksImxhdFRvcCI6NTguMDE2NTk4NTI5NzMxNTMsImxvbkxlZnQiOjU2LjIwMzQ1NDc0OTUzNDY0LCJsb25SaWdodCI6NTYuMjkxODYwMzU4NjY1NDl9LCJ6b29tIjoxNX0%3D")

    # model.train()
    # predictor = ClassificationPredictor(overrides={"transforms":classify_transforms})
    # model.train(t rain_transforms=3)
    # model.train()
    # model.predict("D:\\datasets\\aparts\\prepared_handed\\val\\2\\0b8a7fd7-3703164464_0.jpg", show=True, half=True)
    # time.sleep(5)
