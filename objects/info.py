import json
from dataclasses import dataclass, field, asdict
from typing import List
import os


@dataclass
class BaseInfo:
    __json_opt = dict(default=lambda o: o.__dict__, indent=4, ensure_ascii=False)

    def __str__(self) -> str:
        return json.dumps(asdict(self), **self.__json_opt)

    def to_json_file(self, filepath: str) -> None:
        filepath = filepath.replace(": ", "_", -1)
        dirname = os.path.dirname(filepath)
        if dirname:
            os.makedirs(dirname, exist_ok=True)

        with open(filepath + ".json", 'w', encoding='utf-8') as f:
            json.dump(asdict(self), **self.__json_opt, fp=f)


@dataclass
class EstimatedImage(BaseInfo):
    link: str
    rating: int


@dataclass
class Flat(BaseInfo):
    platform_id: int = -1
    title: str = ''
    link: str = ''
    price: int = -1
    deposit: int = 0
    images: List[str] = field(default_factory=list)
    deal_type: str = ""
    owner: str = ""
    city: str = ""
    platform: str = ""
    commission: int = 0
    description: str = ""
    address: str = ""
    rooms_number: int = 0
    floor: int = -1
    max_floor: int = -1
    house_area: float = -1
    meter_cost: int = -1
