import time

from callbacks.parser_callbacks import ParserCallbacks
from parsers import AvitoParser
from utils.captcha_solver import CaptchaSolver
import traceback

if __name__ == "__main__":
    callbacks = ParserCallbacks(f"D:\\datasets\\aparts\\parsed_handed\\avito",
                                "D:\\datasets\\aparts\\parsed_handed\\avito")
    parser = AvitoParser(callbacks=callbacks, headless=False, captcha_solver=CaptchaSolver())
    parser.webdriver.options.page_load_strategy = "eager"
    hrefs = ["https://www.avito.ru/perm/kvartiry/1-k._kvartira_408_m_1223_et._3078714124"
             ]

    for href in hrefs:
        for i in range(3):
            try:
                flat_id = parser.get_flat_id(href)
                flat = parser.parse_flat(href, flat_id, parse_only_images=False)
                callbacks.on_parse_flat(flat, parser.webdriver.execute_script("return navigator.userAgent;"), None,
                                        f"D:\\datasets\\aparts\\parsed_handed\\avito")
                break
            except:
                traceback.print_exc()
                time.sleep(5)
    # parser.parse_region(
    #     classes_urls.get(class_)
    # )
    parser.stop_parser()
