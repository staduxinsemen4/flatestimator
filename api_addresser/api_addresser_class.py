from urllib.parse import urljoin

from requests import Session

from api_addresser.utils import actual_token
from api_config import email, password, api_url


class ApiAddresser:
    def __init__(self, api_url: str, email: str, password: str):
        self.req_session = Session()
        self.email = email
        self.api_url = api_url
        self.password = password
        self.login_api()

    def login_api(self):
        response = self.req_session.get(urljoin(self.api_url, "login"), data=dict(email=self.email,
                                                                                  password=self.password)).json()
        print(response)
        self.req_session.headers = dict(token=response.get("token"),
                                        refreshToken=response.get("refreshToken"))
        print(self.req_session.headers)
        print("successfully logged in api")

    @actual_token
    def get_flats(self, **kwargs):
        response = self.req_session.get(urljoin(self.api_url, "flat_estimation"), params=kwargs).json()

        return response

    @actual_token
    def set_flats(self, **kwargs):
        response = self.req_session.put((urljoin(self.api_url, "flat_estimation")), json=kwargs).json()

        return response

    @actual_token
    def update_flat(self, id: str, platform: str):
        response = self.req_session.patch((urljoin(self.api_url, "flat_estimation/check_flat")),
                                          params=dict(id=id, platform=platform)).json()

        return response

    def refresh_token(self):
        response = self.req_session.get(urljoin(self.api_url, "refresh_token")).json()
        self.req_session.headers = dict(token=response.get("token"),
                                        refreshToken=response.get("refreshToken"))


if __name__ == "__main__":
    addresser = ApiAddresser(api_url, email, password)
    flats: list = addresser.get_flats(city='Пермь', sort_by="rating", type='daily').get("flats")
    for flat in flats:
        print(flat)
        print(flat.get("rating"))
    # flats:dict = addresser.get_flats(city="Пермь",type='daily',sort_by="rating")
    #
    # flat_data:dict
    # for flat_name,flat_data in flats.items():
    #     print(flat_data)
    #     print(flat_data['rating'])
