


def actual_token(func):
    def wrapper(api, *args, **kwargs):
        res = func(api, *args, **kwargs)
        print(res)
        if res.get("err"):
            api.refresh_token()
            res = func(api, *args, **kwargs)
        return res

    return wrapper
