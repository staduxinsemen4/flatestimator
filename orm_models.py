from datetime import datetime

from peewee import PostgresqlDatabase, Model, CharField, DateTimeField, DateField, BooleanField, BigIntegerField, \
    IntegerField, FloatField, SQL, ForeignKeyField
from playhouse.pool import PooledPostgresqlExtDatabase
from playhouse.postgres_ext import ArrayField

from config import DataBaseConfig

db = PostgresqlDatabase(
    host=DataBaseConfig.host,
    port=DataBaseConfig.port,
    database=DataBaseConfig.database,
    user=DataBaseConfig.user,
    password=DataBaseConfig.password,
    autoconnect=True
)


class BaseModel(Model):
    update_datetime = DateTimeField(default=datetime.utcnow)
    create_datetime = DateTimeField(default=datetime.utcnow)

    def save(self, *args, **kwargs):
        self.update_datetime = datetime.utcnow()
        return super().save(*args, **kwargs)

    class Meta:
        database = db


class User(BaseModel):
    tg_user_id = BigIntegerField(index=True, unique=True, primary_key=True)
    first_name = CharField()
    last_name = CharField(default=None, null=True)
    is_bot = BooleanField()
    language_code = CharField(null=True)
    username = CharField(null=True)
    is_subscriber = BooleanField(default=False)
    last_subscribe_datetime = DateTimeField(null=True)
    subscribe_datetime_end = DateTimeField(null=True)


class BasePlatformModel(BaseModel):
    platform = CharField(max_length=60)


class ParsedFlat(BasePlatformModel):
    platform_id = BigIntegerField()
    rating = FloatField()
    title = CharField(max_length=60)
    price = IntegerField()
    meter_cost = BigIntegerField()
    deposit = IntegerField()
    deal_type = CharField(max_length=60)
    owner = CharField(max_length=60)
    city = CharField(max_length=60)
    commission = IntegerField()
    address = CharField()
    rooms_number = IntegerField()
    floor = IntegerField()
    max_floor = IntegerField()
    house_area = FloatField()
    link = CharField()

    class Meta:
        table_name = 'parsed_flats'
        indexes = (
            (('platform_id', 'platform'), True),
        )


class ParsedImage(BasePlatformModel):
    platform_id = BigIntegerField()
    link = CharField()
    rating = FloatField()
    seq_number = IntegerField()

    class Meta:
        table_name = "parsed_images"
        indexes = (
            (('platform_id', 'platform'), False),
        )
        constraints = [SQL('UNIQUE (platform_id, platform, seq_number)')]


class ParsingRegion(BasePlatformModel):
    tg_user_id = ForeignKeyField(User, index=True)
    url = CharField(max_length=700)
    name = CharField()
    active = BooleanField()
    last_parsed_datetime = DateTimeField(default=None, null=True)
    last_parsed_flats_count = BigIntegerField(default=None, null=True)
    parsed_count = BigIntegerField(default=0)
    polygonX = ArrayField(IntegerField, null=True)
    polygonY = ArrayField(IntegerField, null=True)
    map_position = CharField(null=True)

    class Meta:
        database = db
        table_name = 'parsing_regions'
        constraints = [SQL('UNIQUE (tg_user_id, name)')]


class SendedFlat(BasePlatformModel):
    platform_id = BigIntegerField()
    tg_user_id = ForeignKeyField(User)
    region_id = ForeignKeyField(ParsingRegion, ParsingRegion.id, null=True, on_delete='CASCADE')

    class Meta:
        table_name = 'sended_flats'
        indexes = (
            (('platform_id', 'platform', 'tg_user_id', 'region_id'), True),
        )


class StartedTasks(Model):
    task_started_datetime = DateTimeField(default=datetime.utcnow)
    tg_user_id = ForeignKeyField(User)
    task_name = CharField()

    class Meta:
        database = db
        table_name = 'started_tasks'
        indexes = (
            (('task_started_datetime', 'task_name', 'tg_user_id'), True),
        )


class DeniedFlat(BasePlatformModel):
    platform_id = BigIntegerField()
    tg_user_id = ForeignKeyField(User)
    region_id = ForeignKeyField(ParsingRegion, ParsingRegion.id, null=True, on_delete='CASCADE')

    class Meta:
        table_name = 'denied_flats'
        indexes = (
            (('platform_id', 'platform', 'tg_user_id', 'region_id'), True),
        )


# class DayDetectionTasksModel(BaseModel):
#     task_id = CharField(max_length=60)
#     status = CharField(max_length=60)
#     date = DateField(formats=VideoConfig.API_DAY_FORMAT)
#     camera_id = IntegerField()
#     device = CharField(max_length=60)
#
#     class Meta:
#         table_name = 'day_detection_tasks'
#
#
# class DayVerifyModel(BaseModel):
#     date = DateField(formats=VideoConfig.API_DAY_FORMAT)
#     isvalid = BooleanField(default=True)
#
#     class Meta:
#         table_name = 'validated_days_info'


def init_db():
    db.create_tables([ParsedFlat, ParsedImage, DeniedFlat, SendedFlat, StartedTasks, ParsingRegion, User])


init_db()
