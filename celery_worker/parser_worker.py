from billiard.exceptions import SoftTimeLimitExceeded
from celery import Celery

from urllib3.exceptions import ReadTimeoutError, MaxRetryError
import time
from celery.signals import worker_init

from config import CeleryParserConfig
import sys
from telegram.error import Forbidden

sys.path.append('.')
celery_parser = Celery('flat_estimator_celery_parser')

celery_parser.config_from_object(CeleryParserConfig)
import numpy as np


def restore_all_unacknowledged_messages():
    """
    Restores all the unacknowledged messages in the queue.
    Taken from https://gist.github.com/mlavin/6671079
    """
    conn = celery_parser.connection(transport_options={'visibility_timeout': 0})
    qos = conn.channel().qos
    qos.restore_visible()
    print('Unacknowledged messages restored')


@worker_init.connect
def configure(sender=None, conf=None, **kwargs):
    restore_all_unacknowledged_messages()


@celery_parser.task(bind=True, name="parse_flats",
                    autoretry_for=(ReadTimeoutError, MaxRetryError, SoftTimeLimitExceeded),
                    retry_kwargs={'retry_backoff': 10, 'retry_backoff_max': 30})
def parse_flats(self, user_id: int):
    import torch

    print("cuda: ", torch.cuda.is_available())

    print(torch.cuda.device_count())

    print("current_device: ", torch.cuda.current_device())
    from celery_worker.utils import remove_current_tasks, remove_tasks
    from cls_transforms import patch_inference
    from orm_models import StartedTasks, User
    from telegram_bot.bot_sender import send_region_err_from_task, send_apart_info_start_message
    from telegram_bot.bot_utils import is_subscribe_alive
    import asyncio
    user = User.get(tg_user_id=user_id)
    subscribe_alive, subscribe_time_left = is_subscribe_alive(user)
    if not subscribe_alive:
        try:
            asyncio.run(send_region_err_from_task(user_id,
                                                  f"Ваша подписка закончилась, поиск квартир остановлен\n\n"
                                                  f"Для возобновления поиска купите подписку и запустите поиск снова"))
        except Forbidden:
            print("Блокировка пользователем на моменте снятия подписки, завершаю")
        print(f"отправлено сообщение Ваша подписка закончилась {user_id}")
        remove_tasks(user_id)
        print(f"Задачи завершены {user_id}")

    patch_inference()

    from ultralytics import YOLO
    from analytics import FlatEstimator
    from callbacks.database_telegram_callback import DataBaseTelegramCallback
    import os, traceback
    from utils.captcha_solver import CaptchaSolver
    from utils.region_recoverer import RegionRecoverer
    from telegram_bot.validators import validate_regions_to_run

    StartedTasks.create(tg_user_id=user_id, task_name=self.name)
    regions, err = validate_regions_to_run(user_id)
    if err:
        asyncio.run(send_region_err_from_task(user_id, err))
        print(err)
        remove_tasks(user_id)
        return
    model = YOLO(
        "model/yolov8m-cls.pt_flats_quality_handed_resize_ratio_scale_ext_1221_1000/weights/best.pt", verbose=False)
    print("model/yolov8m-cls.pt_flats_quality_handed_resize_ratio_scale_ext_1221_1000/weights/best.pt")
    estimator = FlatEstimator(model)

    from parsers.avito_parser import AvitoParser
    callback = DataBaseTelegramCallback(estimator, image_download_delay=0.1, task=self, chat_id=user_id)
    parser = None
    for region in regions:
        if not region.active:
            continue

        for i in range(3):
            try:
                if parser is None:
                    parser = AvitoParser(callback, captcha_solver=CaptchaSolver(), region_recoverer=RegionRecoverer())
                print(f"cookies: {parser.webdriver.get_cookies()}")
                time.sleep(3)
                callback.init_region(region.id, region.name)
                res, err = parser.parse_region(region.url, region.name, region.id)
                if err:
                    print(err)
                    asyncio.run(send_region_err_from_task(user_id,
                                                          f"Проблема с регионом #{region.name}\n\n{err}\n\nРегион будет пропущен\n\n"
                                                          f"Измените регион, прислав новую ссылку, либо удалите и создайте новый"))
                    break
                break
            except Forbidden:
                print("Блокировка пользователем, завершаю")
                if parser is not None:
                    parser.stop_parser()
                return
            except Exception as e:
                ex = traceback.format_exc()
                if parser is not None:
                    parser.send_screenshot_with_message(ex[:250])
                print(ex + f" Попытка {i}")

    if parser is not None:
        parser.stop_parser()
    if callback.parsed_aparts and not callback.just_inited:
        send_task_name = "send_parsed_flats"

        # for region_id, region_info in callback.parsed_aparts.items():
        #     if not region_info["aparts"]: continue
        #     average_rating = sum([apart['rating'] for apart in region_info["aparts"]]) / len(region_info["aparts"])
        #     aparts_sorted = sorted(region_info["aparts"], key=lambda apart: apart['rating'], reverse=True)
        #     all_aparts_rating = [apart['rating'] for apart in aparts_sorted]
        #     p = np.percentile(all_aparts_rating, 60)
        #     best_aparts = [apart for apart in region_info["aparts"] if apart['rating'] > average_rating]
        #     best_aparts_percentile = [apart for apart in region_info["aparts"] if apart['rating'] > p]
        #     print(
        #         f"Средний рейтинг для региона {region_info.get('region_name')}: {average_rating} количество лучших квартир: {len(best_aparts)} всего квартир: {len(region_info['aparts'])}")
        #     print(
        #         f"пороговый рейтинг для региона по перцентилю {region_info.get('region_name')}: {p} количество лучших квартир по перцентилю: {len(best_aparts_percentile)} всего квартир: {len(region_info['aparts'])}")
        remove_current_tasks(user_id, send_task_name)
        time.sleep(6)

        celery_parser.send_task(send_task_name, kwargs={"regions_aparts": callback.parsed_aparts, "user_id": user_id})
