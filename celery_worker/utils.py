import traceback

import redbeat
from redbeat import RedBeatSchedulerEntry, RedBeatScheduler
from redbeat.schedulers import RedBeatConfig
import celery


def get_schedules() -> dict:
    from celery_worker.parser_worker import celery_parser
    # from celery_worker.sender_worker import celery_sender
    config = RedBeatConfig(celery_parser)
    schedule_key = config.schedule_key

    redis = redbeat.schedulers.get_redis(celery_parser)

    elements = redis.zrange(schedule_key, 0, -1, withscores=False)

    entries = {el: RedBeatSchedulerEntry.from_key(key=el, app=celery_parser) for el in elements}

    return entries


def remove_current_tasks(user_id: int, task_name: str = None):
    from celery_worker.parser_worker import celery_parser
    tasks_c = celery_parser.control.inspect()

    current_user_tasks = [tasks_c.scheduled(), tasks_c.active(), tasks_c.reserved()]

    for tasks_info in current_user_tasks:
        try:
            for worker, tasks in tasks_info.items():

                for type_task in tasks:
                    if task_name:
                        if type_task["name"] != task_name:
                            continue
                    if type_task["kwargs"].get("user_id") == user_id:
                        celery_parser.control.revoke(type_task["id"], terminate=True, signal='SIGKILL')
        except:
            print(traceback.format_exc())
            pass


def remove_periodic_tasks(user_id: int):
    from celery_worker.parser_worker import celery_parser
    # from celery_worker.sender_worker import celery_sender

    redbeat_scheduler = RedBeatScheduler(app=celery_parser)

    # print(tasks_c.scheduled().values()[0])
    # print(tasks_c.active().values()[0])
    # print(tasks_c.reserved().values()[0])
    # for task in celery.control.inspect():
    #     print(task)

    print(get_schedules(), "schedules before delete")

    print(user_id)
    print((get_schedules()).keys())
    task_keys_to_remove = [task_key for task_key in (get_schedules()).keys() if str(user_id) in task_key]
    print(task_keys_to_remove)
    for task in task_keys_to_remove:
        entry = redbeat_scheduler.Entry.from_key(key=task, app=celery_parser)
        print(entry.kwargs)
        print("kwargs")
        entry.delete()
    print(get_schedules(), "schedules after delete")
    # log_schedule_execution(self)


def check_user_active_schedules(chat_id: int) -> bool:
    schedules = get_schedules()
    for name, schedule_entry in schedules.items():
        if schedule_entry.kwargs["user_id"] == chat_id and schedule_entry.enabled:
            return True
    return False


def remove_tasks(chat_id):
    remove_periodic_tasks(chat_id)
    remove_current_tasks(chat_id)

