import time
import traceback
import sys

from peewee import OperationalError as PeeweeOperationalError, DoesNotExist
from psycopg2 import OperationalError as PsycopgOperationalError
from telegram.error import RetryAfter, BadRequest
import numpy as np

sys.path.append('.')
import asyncio
from celery import Celery

from config import CelerySenderConfig

from time import sleep

celery_sender = Celery('flat_estimator_celery_sender')
from scipy import stats

celery_sender.config_from_object(CelerySenderConfig)


@celery_sender.task(bind=True, name="send_parsed_flats",
                    autoretry_for=(PeeweeOperationalError, PsycopgOperationalError))
def send_parsed_flats(self, regions_aparts: dict[int, dict[str, list]], user_id):
    from orm_models import SendedFlat
    from telegram_bot.bot_sender import send_apart_info, send_apart_info_start_message
    region_rating_lists = slice_flats_by_percentile(regions_aparts, user_id, 75)
    region_init_lens = {region_id: len(region_info.get("aparts")) for region_id, region_info in regions_aparts.items()}
    total_aparts = sum(region_init_lens.values())
    region_counters = dict()
    total_sended_flats = 0
    message_sended = False
    while any([region_info['aparts'] for region_info in regions_aparts.values()]):
        for region_id, region_info in regions_aparts.items():
            if not len(region_info.get("aparts")):
                continue
            if not message_sended:
                asyncio.run(send_apart_info_start_message(user_id))
                message_sended = True
            if region_id not in region_counters:
                region_counters[region_id] = 0
                region_info['aparts'] = sorted(region_info["aparts"], key=lambda apart: apart['rating'], reverse=True)
            apart = region_info["aparts"].pop(0)
            print(f"осталось {len(region_info['aparts'])}")
            region_counters[region_id] += 1
            flood_exc = False
            sended = False
            for p in range(4):
                try:
                    if region_rating_lists:
                        region_rating_list = region_rating_lists.get(region_id, [])
                        if region_rating_list:
                            flat_percentile = stats.percentileofscore(region_rating_list, apart['rating'])
                            print(
                                f"Перцентиль квартиры: {flat_percentile} рейтинг: {apart['rating']} ссылка: {apart['flat_info']['link']} ")
                    asyncio.run(send_apart_info(apart, region_init_lens.get(region_id), region_counters[region_id],
                                                region_info.get('region_name'), chat_id=user_id, region_id=region_id
                                                ))
                    SendedFlat.create(tg_user_id=user_id, platform_id=apart['flat_info']['platform_id'],
                                      platform=apart['flat_info']['platform'], region_id=region_id)
                    sended = True
                    flood_exc = False
                    break
                except (RetryAfter, BadRequest):
                    flood_exc = True
                    print(traceback.format_exc())
                except Exception:
                    print(traceback.format_exc())
                    time.sleep(4)
            if not sended:
                print(f"Failed to send apart info {apart['flat_info']['link']} user_id:{user_id} region_id:{region_id}")
                print(flood_exc)
                if flood_exc:
                    print(f"завершаю задачу send_parsed_flats user_id:{user_id} region_id:{region_id}")
                    return
                continue
            print(f"sended {apart['flat_info']['link']} user_id:{user_id} region_id:{region_id}")
            total_sended_flats += 1

            sleep_time = min(max(28, round(0.2 * total_aparts)), 360)
            # if total_sended_flats <= 50:
            #     sleep_time = min(60, sleep_time)
            sleep(sleep_time)


def slice_flats_by_percentile(regions_aparts_to_send: dict[int, dict[str, list]], user_id, percentile: int):
    from orm_models import SendedFlat, ParsedFlat
    from playhouse.shortcuts import model_to_dict

    region_rating_lists = {}
    for region_id, region_info in regions_aparts_to_send.items():
        all_region_aparts = []
        region_parsed_flats_without_duplicates = []
        region_sended_flats = []
        try:
            region_sended_flats = SendedFlat.select().where(
                (SendedFlat.tg_user_id == user_id) & (
                        SendedFlat.region_id == region_id)).dicts()

            for sended_flat in region_sended_flats:
                print(sended_flat)
                db_flat = ParsedFlat.get_or_none(platform_id=sended_flat['platform_id'])
                if db_flat is not None:
                    db_flat = model_to_dict(db_flat)
                    all_region_aparts.append(db_flat)

        except DoesNotExist:
            pass
        for apart in region_info.get("aparts"):
            sended_flat = list(
                filter(lambda flat: flat['platform_id'] == apart['flat_info']['platform_id'], region_sended_flats))
            if len(sended_flat) >= 1:
                print(f"дубль: {sended_flat}:{apart}")
                continue
            region_parsed_flats_without_duplicates.append(apart)
            all_region_aparts.append(apart)
        region_info['aparts'] = region_parsed_flats_without_duplicates
        if len(all_region_aparts) >= 20:
            all_aparts_sorted = sorted(all_region_aparts, key=lambda apart: apart['rating'], reverse=True)
            all_aparts_region_rating_list = [apart['rating'] for apart in all_aparts_sorted]

            percentile_rating_threshold = np.percentile(all_aparts_region_rating_list, percentile)
            best_flats_by_percentile = [apart for apart in region_info['aparts'] if
                                        apart['rating'] > percentile_rating_threshold]

            average_rating = sum([apart['rating'] for apart in all_region_aparts]) / len(all_region_aparts)
            best_flats_by_average = [apart for apart in region_info['aparts'] if apart['rating'] > average_rating]

            print(
                f"Средний рейтинг для региона {region_info.get('region_name')}: {average_rating} количество лучших квартир:"
                f" {len(best_flats_by_average)} всего новых квартир: {len(region_info['aparts'])} всего квартир: {len(all_region_aparts)}")
            print(
                f"Рейтинг по перцентилю {percentile} для региона {region_info.get('region_name')}: {percentile_rating_threshold} количество лучших квартир:"
                f" {len(best_flats_by_percentile)} всего новых квартир: {len(region_info['aparts'])} всего квартир: {len(all_region_aparts)}")
            region_info['aparts'] = best_flats_by_percentile
            region_rating_lists[region_id] = all_aparts_region_rating_list
    return region_rating_lists
