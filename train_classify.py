from cls_transforms import patch_train

patch_train()

from ultralytics import YOLO

if __name__ == '__main__':
    model_name = "yolov8m-cls.pt"
    dataset_name = "flats_quality"

    model = YOLO(model_name)

    model.train(data=f'D:\\datasets\\aparts\\prepared_handed', epochs=1000,
                batch=64, pretrained="yolov8m-cls.pt", imgsz=416, project=dataset_name,
                name=f'{model_name}_{dataset_name}_handed_resize_ratio_scale_ext_1221_1000_pretrained',
                resume=False, patience=1000, plots=True, shear=40, perspective=0.001, degrees=40, erasing=0.6, seed=200,
                )

    # freeze_support()
