FROM python:3.11.9-slim-bullseye

RUN apt-get update --fix-missing -y && apt-get upgrade -y &&\
    pip install --no-cache-dir --upgrade pip && \
    apt-get install -y locales ffmpeg libsm6 libxext6

RUN apt-get update --fix-missing && apt-get install -y \
    xvfb \
    x11vnc \
    fluxbox \
    wget \
    unzip \
    gnupg \
    htop\
    --no-install-recommends \
    && rm -rf /var/lib/apt/lists/*


RUN apt-get update --fix-missing && apt-get install -y xterm

RUN apt-get update --fix-missing && apt-get install -y wget gnupg2 apt-utils --no-install-recommends \
    && wget --no-check-certificate https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb \
    && dpkg -i google-chrome-stable_current_amd64.deb || true \
    && apt-get install -fy \
    && rm -rf /var/lib/apt/lists/* google-chrome-stable_current_amd64.deb \
    && which google-chrome-stable || (echo 'Google Chrome was not installed' && exit 1)

# Install x11vnc
RUN mkdir ~/.vnc
RUN x11vnc -storepasswd 1234 ~/.vnc/passwd

WORKDIR flat_estimator

COPY requirements.txt ./requirements.txt

RUN pip3 install --no-cache-dir -r requirements.txt
RUN pip3 install --no-cache-dir torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu124
COPY . .

ENV PYTHONUNBUFFERED=1