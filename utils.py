import os
import json
import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
import shutil
import random
import torchvision.transforms as T
# from albumentations import LongestMaxSize, ShiftScaleRotate
from multiprocessing import Pool
from transforms import SquarePad
import cv2
from itertools import repeat

pd.set_option('display.max_rows', 500)
pd.set_option('display.max_columns', 500)
pd.set_option('display.width', 10000)


# import albumentations as A
#
# resize_transforms = [A.LongestMaxSize(256),
#                      A.PadIfNeeded(256, 256, value=0, border_mode=cv2.BORDER_CONSTANT)]
#
# transforms = A.Compose([
#     A.CoarseDropout(max_holes=3,max_height=50,max_width=50,min_height=20,min_width=20),
#     A.Blur(p=0.2),
#     A.MedianBlur(p=0.2),
#     A.ToGray(p=0.08),
#     A.CLAHE(p=0.2),
#     A.RandomBrightnessContrast(p=0.2),
#     A.RandomGamma(p=0.2),
#     ShiftScaleRotate(scale_limit=0.43, shift_limit=0.1, p=0.5, border_mode=cv2.BORDER_CONSTANT),
#     A.ImageCompression(quality_lower=30, quality_upper=100, p=0.25),
# ])
# resize_transforms = A.Compose(resize_transforms)


def extend_dataset_by_images_paths_info(class_path, images_num, max_images_num, augment, dataset_part):
    count_copy = 0
    while max_images_num - images_num:
        count_copy += 1
        for im_full_name in os.listdir(class_path):
            im_path = os.path.join(class_path, im_full_name)
            im_name, im_ext = os.path.splitext(im_path)
            im_copy_path = os.path.join(class_path, f'{im_name}_copy_{count_copy}{im_ext}')
            # if augment and dataset_part == "train":
            #     image = cv2.imread(im_path)
            #     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
            #     augmented = transforms(image=image)
            #
            #     cv2.imwrite(im_copy_path,
            #                 cv2.cvtColor(augmented["image"], cv2.COLOR_RGB2BGR))
            #
            # else:
            shutil.copy(im_path, im_copy_path)
            images_num += 1
            if max_images_num - images_num == 0:
                break


def extend_dataset_classify(input_dir: str, augment=True, user_max_images_num=-1):
    for dataset_part in os.listdir(input_dir):
        images_paths_info = {}
        max_images_num = 0
        max_class_name = ""
        classes_dir = os.path.join(input_dir, dataset_part)
        for class_name in os.listdir(classes_dir):
            images_path = os.path.join(classes_dir, class_name)

            images_num = len(os.listdir(images_path))
            images_paths_info[images_path] = images_num
            if images_num > max_images_num:
                max_images_num = max(images_num, user_max_images_num) if dataset_part == "train" else images_num
                max_class_name = class_name

        with Pool(5) as p:
            p.starmap(extend_dataset_by_images_paths_info,
                      zip(images_paths_info.keys(), images_paths_info.values(), repeat(max_images_num), repeat(augment),
                          repeat(dataset_part)))


def get_samples_from_json(input_json_path: str):
    images, ratings = [], []

    with open(input_json_path) as f:
        json_data = json.load(f)
    for image_data in json_data:
        image_path = image_data["image"]
        relative_path = os.path.relpath(image_path, "/data/upload")
        image_local_path = os.path.join("D:\\datasets\\aparts\\labelstudio\\media\\upload", relative_path)
        images.append(image_local_path)

        ratings.append(image_data["rating"][0]["rating"])
    return images, ratings


def get_samples_from_class_dirs(input_path: str):
    images, ratings = [], []
    for class_name in os.listdir(input_path):
        class_path = os.path.join(input_path, class_name)
        class_images = os.listdir(class_path)
        for im in class_images:
            im_path = os.path.join(class_path, im)
            images.append(im_path)
            ratings.append(class_name)
    return images, ratings


def prepare_dataset_from_data(images: list, class_values: list, out_dir: str, show_charts: bool = False, resize=True,
                              size=256):
    df = pd.DataFrame({"image": images, "class_value": class_values})
    # plt.figure(figsize=(30, 15))

    train = df.groupby("class_value", group_keys=False).apply(lambda df: df.sample(frac=0.8, random_state=200))

    valid = df.drop(train.index)
    df.loc[train.index, f'part'] = "train"
    df.loc[valid.index, f'part'] = "val"

    for index, row in df.iterrows():
        image_name = os.path.basename(row['image'])

        image_out_dir = os.path.join(out_dir, row[f"part"], str(row["class_value"]))
        os.makedirs(image_out_dir, exist_ok=True)
        # if resize:
        #     image = cv2.imread(row['image'])
        #     image = cv2.cvtColor(image, cv2.COLOR_BGR2RGB)
        #     resized = resize_transforms(image=image)
        #     cv2.imwrite(os.path.join(image_out_dir, image_name), cv2.cvtColor(resized["image"], cv2.COLOR_RGB2BGR))
        # else:
        shutil.copy(row['image'], os.path.join(image_out_dir, image_name))
    if show_charts:
        sns.histplot(data=df, x="class_value")
        plt.show()


from pathlib import Path


def create_txt(directory: str, out_txt: str):
    path = Path(directory)
    with open(out_txt, "w") as out_txt_f:
        for file_path in path.rglob('*.png'):
            print(file_path, file=out_txt_f)


if __name__ == "__main__":
    # prepare_dataset_from_json("D:\\datasets\\aparts\\project-6-at-2024-03-10-15-49-8406f2c2.json",
    #                           "D:\\datasets\\aparts\\prepared_new", show_charts=True)
    # images, ratings = get_samples_from_class_dirs("D:\\datasets\\aparts\\parsed")
    # prepare_dataset_from_data(images, ratings, "D:\\datasets\\aparts\\parsed_prepared", show_charts=True)
    # extend_dataset_classify("D:\\datasets\\aparts\\parsed_prepared")
    # images, ratings = get_samples_from_json("D:\\datasets\\aparts\\project-6-at-2024-06-13-23-29-470ca02a.json")
    # prepare_dataset_from_data(images, ratings, "D:\\datasets\\aparts\\prepared_handed", show_charts=True, resize=False)
    #
    # extend_dataset_classify("D:\\datasets\\aparts\\prepared_handed", augment=False, user_max_images_num=-1)
    create_txt("D:\\datasets\\region\\val", "D:\\datasets\\region\\val.txt")
    # extend_dataset_classify("D:\\datasets\\aparts\\prepared\\flats_quality")
    # extend_dataset_classify("D:\\datasets\\aparts\\prepared\\flats_views")
