import os
from dataclasses import dataclass

from tensorflow.python.keras.models import Model

from definitions import ESTIMATOR_GPU_ENABLE
from utils.functions import make_keras_picklable

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import tensorflow as tf

import tensorflow

make_keras_picklable()
if ESTIMATOR_GPU_ENABLE:
    gpus = tf.config.experimental.list_physical_devices('GPU')
    tf.config.experimental.set_memory_growth(gpus[0], True)
else:
    tf.config.set_visible_devices([], 'GPU')
    my_devices = tf.config.experimental.list_physical_devices(device_type='CPU')
    tf.config.experimental.set_visible_devices(devices=my_devices, device_type='CPU')

@dataclass
class FlatEstimatorModel:
    keras_model: Model
    classes: list
    shape: tuple

    def __str__(self):
        return str(self.__dict__)


def get_keras_mobilenet_model(model_path: str, classes_path: str, shape: tuple) -> FlatEstimatorModel:

    model: Model = tensorflow.keras.models.load_model(model_path)

    with open(classes_path, 'r') as classes_f:
        classes: list = [line.rstrip('\n') for line in classes_f]
    return FlatEstimatorModel(keras_model=model, classes=classes, shape=shape)
