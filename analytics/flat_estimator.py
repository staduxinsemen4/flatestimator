import os

import traceback
from typing import List, Dict
from dataclasses import dataclass
import cv2
import numpy as np
from ultralytics import YOLO

os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'
from objects import Flat
import logging
from dataclasses import field


@dataclass
class EstimatedPhoto:
    url: str
    rating: float
    # bgr_image: np.ndarray = None
    # byte_image: bytes = None
    #
    # def draw_debug(self) -> bytes:
    #     height, width, channels = self.bgr_image.shape
    #     colors_map = {1: (0, 0, 255), 2: (255, 0, 255), 3: (255, 0, 0), 4: (255, 255, 0), 5: (0, 255, 0)}
    #     key, color = min(colors_map.items(), key=lambda x: abs(self.rating - x[0]))
    #     drawed_bgr = cv2.putText(self.bgr_image, f"{round(self.rating, 2)}", (int(width / 40), int(height / 25)),
    #                              cv2.FONT_HERSHEY_COMPLEX, 1,
    #                              color, 2,
    #                              lineType=cv2.LINE_AA)
    #     return cv2.imencode('.jpg', drawed_bgr)[1].tobytes()


@dataclass
class EstimatedApart:
    flat_info: Flat = None
    estimated_photos: List[EstimatedPhoto] = field(default_factory=list)
    rating: float = -1.


class FlatEstimator:
    def __init__(self, model: YOLO):
        self.model = model

        logging.info(f"FlatEstimationModel has been init: {self.model}")

    def estimate(self, flat: Flat, imgs_bgr: List[np.ndarray], bytes_imgs: List[bytes], ) -> EstimatedApart:
        apart_summary_rating = 0.
        estimated_photos = []
        if imgs_bgr:
            preds = self.model.predict(imgs_bgr, half=True, verbose=False, imgsz=256)
            # print(self.model.embed(imgs_bgr, half=True, [-1]))
            for res_, url, bgr_image, byte_img in zip(preds, flat.images, imgs_bgr, bytes_imgs):
                image_summary_rating = 0.
                for class_name, prob in zip(res_.names.values(), res_.probs.data):
                    prob = float(prob)
                    image_summary_rating += prob * int(class_name)
                apart_summary_rating += image_summary_rating / len(flat.images)
                estimated_photos.append(
                    EstimatedPhoto(url=url, rating=image_summary_rating))

                # print(f" {url}: {image_summary_rating}")
        if len(flat.images) == 3:
            apart_summary_rating -= 0.5
        if len(flat.images) == 2:
            apart_summary_rating -= 1.
        if len(flat.images) == 1:
            apart_summary_rating -= 2.
        apart_summary_rating = max(apart_summary_rating, 0.0)
        estimated_apart = EstimatedApart(flat_info=flat, estimated_photos=estimated_photos, rating=apart_summary_rating)
        print(apart_summary_rating)
        return estimated_apart

    def estimate_flats(self, flats: list, thresh_num: int, kick_by_average: bool) -> tuple:
        score_mean = self.eval_mean(flats)
        logging.info(f"Средний рейтинг: {score_mean}")
        new_score_mean = score_mean
        excluded_flats = [flat for flat in flats if flat.imgs]
        excluded_flats.sort(reverse=True,
                            key=lambda flat: flat.rating)
        if len(excluded_flats) > thresh_num:
            if kick_by_average:
                excluded_flats = [flat for flat in excluded_flats if flat.rating > score_mean]
            excluded_flats = excluded_flats[:thresh_num]
            new_score_mean = self.eval_mean(excluded_flats)
        return excluded_flats, score_mean, new_score_mean
