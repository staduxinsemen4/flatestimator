import re
import time
import traceback
from datetime import datetime

from selenium.webdriver import Keys, ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.options import PageLoadStrategy
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from tqdm import tqdm

from callbacks.parser_callbacks import ParserCallbacks
from callbacks.base_callbacks import BaseCallbacks
from objects import Flat
from orm_models import ParsingRegion
from parsers import ApartmentsParserBase
from utils.captcha_solver import CaptchaSolver
from utils.functions import find_numbers
import chromedriver_autoinstaller
import requests
from random import uniform

from utils.region_recoverer import RegionRecoverer


class AvitoParser(ApartmentsParserBase):
    def __init__(self, callbacks: BaseCallbacks, headless: bool = True, captcha_solver: CaptchaSolver = None,
                 region_recoverer: RegionRecoverer = None):
        super().__init__(callbacks, headless)
        self.platform = "avito"
        self.current_district_name = ''
        self.num_of_districts: int = -1
        self.current_district_number: int = -1
        self.current_city: str = ''
        self.captcha_solver = captcha_solver
        self.pbar = None
        self.region_recoverer = region_recoverer

    def send_screenshot_with_message(self, mess=None, user_id=None):
        screenshot = self.webdriver.get_screenshot_as_base64()
        self.callbacks.on_screenshot(screenshot, mess, user_id)

    def parse_region(self, region_url: str, region_name: str = None, region_id: int = None, restored: bool = False):
        if region_id is not None:
            region: ParsingRegion = ParsingRegion.get_by_id(region_id)
            user_id = region.tg_user_id.tg_user_id
        else:
            user_id = None
        print(f"parsing {region_name}:{region_url}")
        region_empty = False
        unblocked = False
        cookies_deleted = False
        max_try = 3
        if restored:
            self.send_screenshot_with_message(
                f"Т.к авито не хранит регионы долго, или из за сбоев сайта, моему ИИ приходится их восстанавливать\n\n"
                f"Пожалуйста проверьте что я восстановил регион #{region_name} верно, можно посмотреть по <a href='{region_url}'>ссылке</a>, или по картинке\n"
                f"В случае ошибки вы можете изменить ссылку региона в настройках региона", user_id=user_id)
        for i in range(max_try):
            if not unblocked:
                self.webdriver.get(region_url)

            # if not cookies_deleted:
            #     self.set_cookies("utils/cookies.json")
            #     print(f"cookies after: {self.webdriver.get_cookies()}")
            time.sleep(6.7)
            try:
                flats_num_attr = WebDriverWait(self.webdriver, 5).until(
                    # EC.visibility_of_all_elements_located()
                    EC.presence_of_all_elements_located((By.XPATH, "//div[contains(@class,'styles-snippet')]"))
                )
                break
            except:
                try:
                    self.webdriver.find_element(By.XPATH, "//div[contains(@class,'list-error-root')]")
                    print("регион пустой")
                    region_empty = True
                    break
                except:
                    pass
                unblocked, cookies_deleted = self.try_unblock()
        # self.webdriver.delete_all_cookies()
        try:
            self.webdriver.find_element(By.XPATH, "//div[contains(@class,'map-side-block-new-close')]/button").click()
            time.sleep(2)
        except:
            pass
        region_recoverer_error = ''
        if self.region_recoverer and region_id is not None:
            new_url, region_recoverer_error = self.region_recoverer.cache_or_restore_region(self.webdriver, region)
            if new_url:
                return self.parse_region(new_url, region_name, region_id, restored=True)
        self.send_screenshot_with_message(f"paring region #{region_name}")
        if region_recoverer_error:
            return False, region_recoverer_error
        if region_empty:
            return True, ''
        flats_num = int(
            self.webdriver.find_element(By.XPATH, "//span[contains(@class,'breadcrumbs-count')]").text.replace(
                " ", "").replace("\n", "").replace("\t", ""))
        print(flats_num)
        if flats_num > 960:
            return False, 'Количество квартир в одном регионе не должно превышать 960, попробуйте настроить фильтры для этого региона или уменьшить область'

        self.pbar = tqdm(total=flats_num, desc=f"processing aparts {region_name}:{region_id}")
        try:
            self.webdriver.find_element(By.XPATH,
                                        "//*[contains(text(), 'Так отмечены объявления, в которых можно посмотреть планировку')]").click()
            print("кликнул")
        except:
            pass
        container = self.webdriver.find_element(By.XPATH, "//div[contains(@class,'side-block-wrapper')]")

        parsed_num = 0
        flats_a = []
        parsed_href = []

        while parsed_num < len(flats_a) or not parsed_num:
            container.click()
            flats_a = container.find_elements(By.XPATH,
                                              "//div[contains(@class,'styles-snippet')]//a[@data-marker='title']")

            current_flat = flats_a[parsed_num]
            href = current_flat.get_attribute("href")
            if href in parsed_href:
                continue

            flat_id = self.get_flat_id(href)
            parsed_href.append(href)
            parsed_num += 1
            self.pbar.update()
            print(parsed_num)
            print(len(flats_a), "len")

            estimated_apart, skip = self.callbacks.before_parse_flat(flat_id, self.platform, region_id)
            if skip:
                ActionChains(self.webdriver).send_keys(Keys.PAGE_DOWN).perform()
                time.sleep(.3)
                continue
            if not estimated_apart:
                main_window = self.webdriver.current_window_handle

                self.webdriver.execute_script("window.open('')")

                self.webdriver.switch_to.window(self.webdriver.window_handles[1])

                self.webdriver.options.page_load_strategy = PageLoadStrategy.eager

                for i in range(3):
                    try:
                        estimated_apart = self.parse_flat(href, flat_id)
                        break
                    except Exception as e:
                        ex = traceback.format_exc()
                        time.sleep(.3)
                        continue
                self.webdriver.close()

                self.webdriver.options.page_load_strategy = PageLoadStrategy.normal
                self.webdriver.switch_to.window(main_window)

            if estimated_apart:
                self.callbacks.on_parse_flat(estimated_apart,
                                             self.webdriver.execute_script("return navigator.userAgent;"),
                                             region_name, region_id)
            ActionChains(self.webdriver).send_keys(Keys.PAGE_DOWN).perform()
            time.sleep(.3)

        if region_id is not None:
            region.last_parsed_flats_count = len(self.callbacks.parsed_aparts.get(region.name, []))
            region.parsed_count += 1
            region.last_parsed_datetime = datetime.utcnow()
            region.save()
        return True, ''

    def parse_flat(self, url: str, flat_id: int, parse_only_images: bool = False):
        print(f"Скачиваю {url}")

        if not self.callbacks.check_unic(flat_id):
            print(f"{flat_id} уже спаршена, пропуск")
            return None

        for i in range(3):
            flat = Flat(platform=self.platform, platform_id=flat_id, link=url)

            self.webdriver.get(url)
            try:
                flat.title = self.webdriver.find_element(By.CLASS_NAME, "style-titleWrapper-Hmr_5").text
                if not flat.title:
                    return None
                break
            except Exception as e:
                ex = traceback.format_exc()
                self.try_unblock()
        if parse_only_images:
            try:
                self.webdriver.find_element(By.XPATH, "//div[contains(@class,'close-extended-button-block')]").click()
            except:
                pass
        if not parse_only_images:
            try:
                self.webdriver.find_element(By.XPATH, "//div[contains(@class,'closed-warning-block')]")
                print("Объявление снято")
                return None
            except:
                pass
            try:
                self.webdriver.find_element(By.XPATH,
                                            "//*[contains(text(), 'Объявление не посмотреть')]")
                print("Объявление не посмотреть")
                return None
            except:
                pass
            # try:
            #     self.webdriver.find_element()
            price_text = (self.webdriver.find_element(By.XPATH, "//span[@class='style-price-value-string-rWMtx']")
                          .get_attribute("textContent"))
            print(price_text)
            if "сутки" in price_text:
                flat.deal_type = DealType.DAILY
            elif "месяц" in price_text:
                flat.deal_type = DealType.RENT
            else:
                flat.deal_type = DealType.BUY

            flat.price = int(
                self.webdriver.find_element(By.XPATH, "//span[@data-marker='item-view/item-price']").get_attribute(
                    "content"))
            if flat.deal_type != DealType.DAILY:
                sub_price = self.webdriver.find_element(By.CLASS_NAME, "style-item-price-sub-price-_5RUD")
            if flat.deal_type == DealType.RENT:
                try:
                    sub_price_divs = sub_price.find_element(By.CSS_SELECTOR, "span")
                    splitted = sub_price_divs.text.split(",")
                    commission_str = ''
                    if len(splitted) == 2:
                        deposit_str, commission_str = splitted
                    else:
                        deposit_str = splitted[0]
                    if commission_str:
                        commission_str = commission_str.replace(" ", "")
                    deposit_str = deposit_str.replace(" ", "")
                    commission_str_nums = find_numbers(commission_str)
                    deposit_str_nums = find_numbers(deposit_str)
                    if commission_str_nums:
                        flat.commission = commission_str_nums[0]
                    if deposit_str_nums:
                        flat.deposit = deposit_str_nums[0]

                except:
                    pass

            elif flat.deal_type == DealType.DAILY:
                flat.deposit = find_numbers(
                    self.webdriver.find_element(By.XPATH,
                                                "//li[contains(@class,'params-paramsList__item')]"
                                                "[span/text()='Залог']").text)[0]
            elif flat.deal_type == DealType.BUY:
                flat.meter_cost = find_numbers(sub_price.text)[0]

            try:
                owner_name = self.webdriver.find_element(By.XPATH,
                                                         "//div[@data-marker='seller-info/label']").text
                print(owner_name)
            except Exception as e:
                print(traceback.format_exc())
                owner_name = "Агенство"
                flat.owner = OwnerType.AGENCY
            if owner_name in ["Арендодатель", "Частное лицо"]:
                flat.owner = OwnerType.PRIVATE
            elif owner_name == "Риелтор":
                flat.owner = OwnerType.REALTOR
            elif owner_name == "Агентство":
                flat.owner = OwnerType.AGENCY
            elif owner_name == "Застройщик":

                flat.owner = OwnerType.DEVELOPER

            flat.address = self.webdriver.find_element(By.XPATH,
                                                       "//span[@class='style-item-address__string-wt61A']").text

            flat.floor, flat.max_floor = find_numbers(
                self.webdriver.find_element(By.XPATH,
                                            "//li[contains(@class,'params-paramsList__item')][span/text()='Этаж']").text)

            try:
                flat.rooms_number = find_numbers(
                    self.webdriver.find_element(By.XPATH,
                                                "//li[contains(@class,'params-paramsList__item')]"
                                                "[span/text()='Количество комнат']").text)[0]
            except:
                pass

            flat.house_area = find_numbers(
                self.webdriver.find_element(By.XPATH,
                                            "//li[contains(@class,'params-paramsList__item')]"
                                            "[span/text()='Общая площадь']").text)[0]

        imgs_urls = self.parse_images()
        if not imgs_urls:
            return None
        flat.images = imgs_urls

        return flat

    def parse_images(self):
        try:
            self.webdriver.find_element(By.XPATH,
                                        "//div[contains(@class,'image-frame-wrapper')]").click()

        except:
            images_finded = False
            for index in [-1, -2, -3]:
                try:
                    imgs_previews = self.webdriver.find_elements(By.XPATH,
                                                                 "//li[contains(@class,'images-preview-previewImageWrapper')]")
                    imgs_previews[index].click()
                    self.webdriver.find_element(By.XPATH,
                                                "//div[contains(@class,'image-frame-wrapper')]").click()
                    images_finded = True
                    break
                except:
                    ActionChains(self.webdriver).send_keys(Keys.ESCAPE).perform()
                    continue
            if not images_finded:
                print("Нет изображений")
                return []

        images = self.webdriver.find_elements(By.XPATH, "//div[contains(@class,'styles-img-wrapper')]")
        num_of_images = len(images)
        print(num_of_images)
        print(f"Кол-во изображений квартиры: {num_of_images}")
        imgs = []
        for i in range(num_of_images):
            time.sleep(.1)
            try:
                im_src = self.webdriver.find_element(By.XPATH,
                                                     "//img[contains(@class,'styles-extended-gallery-img')]").get_attribute(
                    "src")
            except:
                continue
            # if "http" not in data_url:
            #     data_url = "http:" + data_url

            imgs.append(im_src)
            try:
                self.webdriver.find_element(By.XPATH,
                                            "//div[@data-marker='extended-gallery-frame/control-right']").click()
            except:
                continue
        print("Изображения скачаны")
        return imgs

    def get_flat_id(self, url: str):
        return int(re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",
                              url)[-1])

    def try_unblock(self):
        try:
            solved, attempt_num = self.captcha_solver.solve_all(self.webdriver)
            if solved:
                self.send_screenshot_with_message(f"{attempt_num} капчей на регион решено")
                return True, False
            else:
                print("Страница блокировки без капчи, удаляю куки")
                self.webdriver.delete_all_cookies()
                return False, True
        except:
            ex = traceback.format_exc()
            print(ex)
        return False, False


class DealType:
    DAILY = "daily"
    RENT = "rent"
    BUY = "buy"


class OwnerType:
    AGENCY = "agency"
    PRIVATE = "private"
    REALTOR = "realtor"
    DEVELOPER = "developer"
