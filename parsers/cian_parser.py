import re
from multiprocessing import Pool

from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from callbacks.base_callbacks import BaseCallbacks
from parsers import ApartmentsParserBase


class CianParser(ApartmentsParserBase):
    def __init__(self, chromedriver_path: str, callbacks: BaseCallbacks, headless: bool = True):
        self.loader_pool = Pool(processes=6)
        super().__init__(chromedriver_path, callbacks, headless)

    def parse(self, output_url: str):
        print(f"parsing with user-agent: {self.webdriver.execute_script('return navigator.userAgent;')}")
        self.output_url = output_url
        self.webdriver.get(output_url)
        self.page_n = 1
        flats_num = self.eval_num_of_flats()
        self.summary_parsed_time = 0
        print(f"Парсинг: url - {output_url}\nКол-во квартир: {flats_num}")
        self.data_flat_estimator.on_parse(flats_num, platform="cian")
        while True:
            if not self.check_page():
                break
            self.parse_page()
            if not self.get_next_page():
                print("cant load next page")
                break
        self.stop_parser()

    def check_page(self) -> bool:
        try:
            WebDriverWait(self.webdriver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "_93444fe79c--breadcrumbs--3q1Ip"))
            )
        except:
            return False
        try:
            self.webdriver.find_element_by_xpath("//aside[@data-name = 'PreInfiniteBanner']")
            print(f"На {self.page_n} странице достигнут лимит недвижимости")
            return False
        except:
            pass
        return True

    def parse_images(self):
        try:
            WebDriverWait(self.webdriver, 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "fotorama__nav__frame"))
            )
        except:
            print("Нет фотографий")
            return []
        num_of_images = len(self.webdriver.find_elements_by_class_name("fotorama__nav__frame"))
        print(f"Кол-во изображений квартиры: {num_of_images}")
        try:
            self.webdriver.find_element_by_xpath("//div[@data-name = 'CloseIcon']").click()
        except:
            pass
        self.webdriver.find_element_by_class_name("a10a3f92e9--icon-photo--2LYho").click()
        if not self.try_fotorama_click(): return
        imgs_urls = []
        for i in range(num_of_images):
            img_src = self.try_get_image()
            if img_src:
                imgs_urls.append(img_src)
            actions = ActionChains(self.webdriver)
            actions.send_keys(Keys.ARROW_RIGHT)
            actions.perform()

        return imgs_urls

    def parse_page(self):
        print(f"Парсинг страницы {self.page_n}")

        flats = self.webdriver.find_elements_by_class_name("_93444fe79c--link--39cNw")
        self.iterate_flats(flats)

    def parse_flat(self, url: str):
        imgs_urls = self.parse_images()
        if not imgs_urls: return
        current_flat_id = re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",
                                     url)[-1]
        self.data_flat_estimator.on_parse_flat(url, current_flat_id, self.user_agent_randomizer.random, imgs_urls)

    def try_fotorama_click(self) -> bool:
        for i in range(3):
            try:
                WebDriverWait(self.webdriver, 2).until(
                    EC.element_to_be_clickable((By.CLASS_NAME, "fotorama__fullscreen-icon"))
                ).click()
                return True
            except Exception as e:
                print(e, "Fotorama")
                continue
        return False

    def try_get_image(self):
        for i in range(2):
            try:
                time.sleep(0.7)
                img_src = self.webdriver.find_element_by_class_name("fotorama__active") \
                    .find_element_by_css_selector("img").get_attribute("src")
                return img_src
            except Exception as e:
                print(e, "Exception")
                print(self.webdriver.find_element_by_class_name("fotorama__active").get_attribute('innerHTML'))
                continue
        print("Пропускаем картинку")
        return ''

    def eval_num_of_flats(self) -> int:
        try:
            text = self.webdriver.find_element_by_xpath("//div[@data-name = 'SummaryHeader']").text
        except:
            print("Выдача квартир - пустая")
            self.stop_parser()
        else:
            text = text.replace(" ", "", -1)
            num_of_flats = int(re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",
                                          text)[0])
            return num_of_flats

    def get_next_page(self, delimitor: str = "&") -> bool:
        self.page_n += 1
        self.webdriver.get(self.output_url + f"{delimitor}p={self.page_n}")
        return f"{delimitor}p={self.page_n}" in self.webdriver.current_url
