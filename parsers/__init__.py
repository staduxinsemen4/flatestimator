from .parser_base import ParserBase,ApartmentsParserBase
from .avito_parser import AvitoParser
from .cian_parser import CianParser

__all__ = ["ParserBase","AvitoParser","CianParser"]