import time

from fake_useragent import UserAgent
from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
import undetected_chromedriver as uc
from callbacks.base_callbacks import BaseCallbacks
import json
import os
from utils.proxy_plugin import proxies_plug


class ParserBase:
    def __init__(self, headless: bool = False):
        self.user_agent_randomizer: UserAgent = UserAgent(platforms='pc')
        self.webdriver_init(headless)
        # self.webdriver.options.a
        # self.webdriver.fullscreen_window()
        # self.webdriver.implicitly_wait(10)

    def set_cookies(self, cookies_json_file_path: str):
        with open(cookies_json_file_path, 'r') as f:
            data = json.load(f)
            for cookie in data:
                expiry = cookie.get('expiry')
                if expiry is not None:
                    expiry = expiry * 10000
                    print(expiry)
                    cookie['expiry'] = expiry

                print(cookie)
                self.webdriver.add_cookie(cookie)

    def webdriver_init(self, headless):
        chrome_options = uc.ChromeOptions()
        # chrome_options.add_argument("--start-maximized")
        # chrome_options.add_argument("--mute-audio")
        # chrome_options.add_argument("--disable-notifications")
        # chrome_options.add_argument("--disable-popup-blocking")

        # chrome_options.add_argument(f"--user-agent={self.user_agent_randomizer.random}")

        # chrome_options.arguments.extend(["--no-sandbox", "--disable-setuid-sandbox", "--disable-dev-shm-usage"])
        # chrome_options.add_argument("start-maximized")
        # chrome_options.add_argument("enable-automation")
        chrome_options.add_argument("disable-features=NetworkService")
        chrome_options.add_argument('--disable-dev-shm-usage')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--disable-setuid-sandbox')
        chrome_options.add_argument('--disable-notifications')
        chrome_options.add_argument('--disable-features=PrivacySandboxSettings4')
        chrome_options.add_argument('--disable-browser-side-navigation')
        # chrome_options.page_load_strategy = "eager"

        # chrome_options.add_argument("--disable-popup-blocking")
        # proxy_options = {
        #     'proxy': {
        #         'http': "http://P07DS5:o0aUgJ@5.101.86.42:8000",
        #         'https': "http://P07DS5:o0aUgJ@5.101.86.42:8000"
        #     }
        # }
        proxy_plug_path = os.path.join(os.getcwd(), 'plugins/proxy')
        proxies_plug('hAZsAr', 'SejyPuZec7EK', 'fproxy.site', '14957', proxy_plug_path)
        # chrome_options.add_extension(proxies_extension)
        # chrome_options.add_argument(f'--proxy-server=fproxy.site:14957')

        chrome_options.add_argument(
            f'--load-extension={os.path.join(os.getcwd(), "plugins/uBlock0.chromium")}')

        #

        # print(proxy)
        # seleniumwire_options = {
        #     'proxy': {
        #         'http': proxy,  # user:pass@ip:port
        #         'https':  proxy,
        #         'no_proxy': 'localhost,127.0.0.1'
        #     }}
        # prefs = {"profile.default_content_setting_values.notifications": 2}
        # prefs["profile.default_content_settings"] = {"popups": 1}
        # chrome_options.add_experimental_option("prefs", prefs)
        # chrome_options.add_argument('log-level=3')
        # chrome_options.add_argument("window-size=1920x1080")
        # chrome_options.add_option('excludeSwitches', ['enable-logging'])
        # chrome_options.add_experimental_option("detach", True)
        # capabilities = DesiredCapabilities.CHROME.copy()
        # capabilities['acceptSslCerts'] = True
        # capabilities['acceptInsecureCerts'] = True
        chrome_options.add_argument("--disable-gpu")
        # chrome_options.add_argument("--disable-cookies")
        chrome_options.add_argument("--dns-prefetch-disable")
        chrome_options.add_argument("--no-zygote")
        chrome_options.add_argument("--no-zygote-sandbox")
        # chrome_options.add_argument(f'--load-extension={os.path.join(os.getcwd(), "plugins/uBlock0.chromium")}')
        if headless:
            chrome_options.add_argument("--headless=new")
        self.webdriver = uc.Chrome(options=chrome_options, version_main=125, user_multi_procs=True
                                   )
        self.webdriver.set_window_size(2560, 1440)
        # self.webdriver.set_window_size(1920, 1080)
        # self.webdriver.maximize_window()

        self.webdriver.command_executor.set_timeout(18)
        self.webdriver.set_page_load_timeout(18)
        self.webdriver.set_script_timeout(80)
        print(f'cookies before: {self.webdriver.get_cookies()}')
        # self.set_cookies("utils/cookies.json")

        # print(user_agent)

    def stop_parser(self):
        try:
            time.sleep(3)
            self.webdriver.close()
            time.sleep(3)
            self.webdriver.quit()
        except:
            print("already closed")
        try:
            pid = True
            while pid:
                pid = os.waitpid(-1, os.WNOHANG)
                print("Reaped child: %s" % str(pid))
                print("sdadadsds")
                # Wonka's Solution to avoid infinite loop cause pid value -> (0, 0)
                try:
                    if pid[0] == 0:
                        pid = False
                except:
                    pass
                # ---- ----

        except ChildProcessError:
            pass

    def __del__(self):
        print("in destructor")
        self.stop_parser()


class ApartmentsParserBase(ParserBase):
    def __init__(self, callbacks: BaseCallbacks, headless: bool = True):
        super().__init__(headless)
        self.output_folder: str = ''
        self.page_n: int = 1
        self.output_url: str = ''
        self.callbacks = callbacks
        self.platform: str = ''
        self.mode: str = ''

    def parse(self, output_url: str, mode: str):
        print(f"parsing with user-agent: {self.webdriver.execute_script('return navigator.userAgent;')}")
        self.mode = mode
        flats_num = self.eval_num_of_flats()

        print(f"Парсинг: url - {output_url}\nКол-во квартир: {flats_num}")
        self.callbacks.on_parse(flats_num)

    def eval_num_of_flats(self) -> int:
        pass

    def parse_flat(self, url: str, id: str):
        raise NotImplementedError

    def iterate_flats(self, flats):
        for n, flat in enumerate(flats, 1):
            url = flat.get_attribute("href")
            flat_id = self.get_flat_id(url)
            if not self.callbacks.check_unic(flat_id): continue
            self.callbacks.on_start_parse_page_iteration(page_n=self.page_n)

            main_window = self.webdriver.current_window_handle

            self.webdriver.execute_script("window.open(''),'_blank'")

            self.webdriver.switch_to.window(self.webdriver.window_handles[1])

            self.webdriver.get(url)

            self.parse_flat(url, flat_id)

            self.webdriver.close()
            self.webdriver.switch_to.window(main_window)

            print(f"Квартира {n} на странице {self.page_n} ({url}) отпаршена")
            self.callbacks.on_end_parse_page_iteration(self.page_n)

    def get_flat_id(self, url: str):
        raise NotImplementedError
