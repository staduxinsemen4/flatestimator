import multiprocessing

from definitions import CHROME_PATH, CHROMEDRIVERS_PATH, CHROMEDRIVER_DOWNLOAD_SRC, PHANTOMJS_PATH, \
    DATASET_DOWNLOAD_SETTINGS_PATH
from utils.runner_from_settings import run_from_settings_files
from utils.selenium_loader import SeleniumLoader

if __name__ == "__main__":
    multiprocessing.freeze_support()
    loader = SeleniumLoader(chrome_path=CHROME_PATH, chromedrivers_path=CHROMEDRIVERS_PATH,
                            chromedriver_download_src=CHROMEDRIVER_DOWNLOAD_SRC,
                            phantomjs_path=PHANTOMJS_PATH)
    chromedriver_path = loader.check_and_download()

    run_from_settings_files(DATASET_DOWNLOAD_SETTINGS_PATH, chromedriver_path)
