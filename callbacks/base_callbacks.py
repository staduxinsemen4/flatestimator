import time

from typing_extensions import Union

from objects import Flat


class BaseCallbacks:
    def __init__(self, image_download_delay: Union[float, int]):
        self.image_download_delay = image_download_delay
        self.summary_parsed_time: float = .0
        self.start_page_iteration_time: float = .0
        self.page_iteration_parsing_time: float = .0
        self.parsed_flats_counter: int = 0
        self.flats_num: int = -1

    def before_parse_flat(self, flat_id: int, platform: str, region_id:int):
        return None, False

    def on_end_parse_region(self):
        pass

    def on_parse(self, flats_num: int):
        self.flats_num = flats_num

    def on_parse_flat(self, flat: Flat, user_agent: str, region_name: str):
        self.parsed_flats_counter += 1

    def on_start_parse_page_iteration(self, page_n: int):
        self.start_page_iteration_time = time.time()

    def on_end_parse_page_iteration(self, page_n: int):
        self.page_iteration_parsing_time = time.time() - self.start_page_iteration_time
        print(f"{self.page_iteration_parsing_time} s. на итерацию парсинга квартиры")
        self.summary_parsed_time += self.page_iteration_parsing_time
        self.eval_mean_time()

    def check_unic(self, flat_id):
        return True

    def on_screenshot(self, screenshot, mess=None,user_id=None):
        pass

    def eval_mean_time(self):
        mean_flat_parse_time = self.summary_parsed_time / self.parsed_flats_counter
        time_to_end_parsing = int(
            mean_flat_parse_time * (self.flats_num - self.parsed_flats_counter))
        m, s = divmod(time_to_end_parsing, 60)
        h, m = divmod(m, 60)
        print(f"Среднее время парсинга одной недвижимости: {int(mean_flat_parse_time)} s")
        print(f"Примерное время до конца парсинга: {h} час {m} мин {s} сек")
