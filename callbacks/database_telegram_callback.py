import time

from peewee import DoesNotExist

from analytics import FlatEstimator
from callbacks.estimator_callbacks import EstimatorCallbacks
from analytics.flat_estimator import EstimatedPhoto
from objects import Flat

import asyncio

from telegram_bot.bot_sender import send_screenshot
from telegram_bot import admin_chat_id
from analytics.flat_estimator import EstimatedApart
from dataclasses import asdict
from orm_models import ParsedFlat, ParsedImage, db, DeniedFlat, SendedFlat
from playhouse.shortcuts import model_to_dict


class DataBaseTelegramCallback(EstimatorCallbacks):
    def __init__(self, flat_estimator: FlatEstimator, image_download_delay: int | float, task=None, chat_id=None):
        super().__init__(flat_estimator, image_download_delay)
        self.task = task
        self.parsed_aparts: dict[int, dict[str, list]] | dict = dict()
        self.progress_message_id = None
        self.chat_id = chat_id if chat_id else admin_chat_id

        self.average_rating = -1.
        self.average_price = -1.
        self.average_relative_quality = -1.
        self.just_inited = True

    def before_parse_flat(self, flat_id: int, platform: str, region_id: int) -> tuple[None | EstimatedApart, bool]:
        # try:
        #     DeniedFlat.select().where(
        #         (DeniedFlat.platform_id == flat_id) & (DeniedFlat.platform == platform) & (
        #                 DeniedFlat.tg_user_id == self.chat_id)).get()
        #     return None, True
        # except DoesNotExist:
        #     pass
        # sended_flat = SendedFlat.get_or_none(platform_id=flat_id, platform=platform, tg_user_id=self.chat_id,
        #                                      region_id=region_id)
        # if sended_flat is not None:
        #     return None, True
        db_flat = ParsedFlat.get_or_none(platform_id=flat_id, platform=platform)
        if db_flat is None:
            return None, False
        db_flat = model_to_dict(db_flat)

        try:
            db_imgs = ParsedImage.select().where(
                (ParsedImage.platform_id == db_flat['platform_id']) & (
                        ParsedImage.platform == db_flat['platform'])).order_by(ParsedImage.seq_number.asc()).dicts()
        except DoesNotExist:
            return None, False

        estimated_apart = EstimatedApart(rating=db_flat["rating"])
        del db_flat["rating"], db_flat["update_datetime"], db_flat["create_datetime"], db_flat["id"]
        apart_info = Flat(**db_flat)
        estimated_apart.flat_info = apart_info
        for im in db_imgs:
            estimated_img = EstimatedPhoto(url=im['link'], rating=im['rating'])
            estimated_apart.estimated_photos.append(estimated_img)
        return estimated_apart, False

    def on_parse_flat(self, flat: Flat | EstimatedApart, user_agent: str, region_name: str, region_id):
        if isinstance(flat, Flat):
            estimated_apart = super().on_parse_flat(flat, user_agent, region_name, use_pool=True)
            estimated_dict = asdict(estimated_apart)
            base_flat_info = estimated_dict["flat_info"]
            del base_flat_info['images'], estimated_dict['estimated_photos'], estimated_dict['flat_info']
            ParsedFlat.create(**estimated_dict, **base_flat_info)
            for i, im in enumerate(estimated_apart.estimated_photos):
                ParsedImage.create(link=im.url, rating=im.rating, platform_id=estimated_apart.flat_info.platform_id,
                                   platform=estimated_apart.flat_info.platform, seq_number=i)

        else:
            estimated_apart = flat
        if estimated_apart is None:
            return
        # if region_id not in self.parsed_aparts:
        #     self.parsed_aparts[region_id] = dict(region_name=region_name, aparts=[])
        self.parsed_aparts[region_id]['aparts'].append(asdict(estimated_apart))
        self.just_inited = False

    def init_region(self, region_id, region_name):
        self.parsed_aparts[region_id] = dict(region_name=region_name, aparts=[])
        # asyncio.run(send_apart_info(estimated_apart, None, chat_id=self.chat_id))
        # time.sleep(15)

    # def on_end_parse_region(self):
    #     if not self.parsed_aparts:
    #         sleep(7)
    #         return
    #     for region_name, region_aparts in self.parsed_aparts.items():
    #         region_aparts = self.parsed_aparts[region_name]
    #         region_aparts.sort(key=lambda apart: apart.rating, reverse=True)
    #         for i, apart in enumerate(region_aparts, start=1):
    #             asyncio.run(send_apart_info(apart, len(self.parsed_aparts), i, region_name, chat_id=self.chat_id,
    #                                         ))
    #             sleep(30)

    def get_average_rating(self):
        average_rating = sum([apart.rating for apart in self.parsed_aparts]) / len(self.parsed_aparts)
        # average_price = sum([apart.flat_info.price for apart in self.parsed_aparts]) / len(self.parsed_aparts)
        # average_relative_quality = sum(
        #     [apart.rating / apart.flat_info.price for apart in self.parsed_aparts]) / len(self.parsed_aparts)

        return average_rating

    def check_unic(self, flat_id):
        print("here")
        return True

    def on_screenshot(self, screenshot, mess=None, user_id=None):
        asyncio.run(send_screenshot(screenshot, mess, chat_id=admin_chat_id if user_id is None else user_id))
