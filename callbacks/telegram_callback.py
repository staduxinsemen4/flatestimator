import time

from analytics import FlatEstimator
from callbacks.estimator_callbacks import EstimatorCallbacks
from objects import Flat

import asyncio
from typing import List, Union

from telegram_bot.bot_sender import send_apart_info, send_loading_info, send_screenshot
from telegram_bot import admin_chat_id
from analytics.flat_estimator import EstimatedApart
from tqdm import tqdm
import datetime
from time import sleep


class TelegramCallback(EstimatorCallbacks):
    def __init__(self, flat_estimator: FlatEstimator, image_download_delay: Union[int, float], task=None, chat_id=None):
        super().__init__(flat_estimator, image_download_delay)
        self.task = task
        self.region_aparts: List[EstimatedApart] = []
        self.pbar = None
        self.progress_message_id = None
        self.chat_id = chat_id if chat_id else admin_chat_id

    def on_start_parsing_region(self, region_url: str, flats_num: int) -> None:
        super().on_start_parsing_region(region_url, flats_num)
        self.pbar = tqdm(total=self.flats_num, desc="processing aparts")

    def on_parse_flat(self, flat: Flat, user_agent: str):
        estimated_apart = super().on_parse_flat(flat, user_agent)
        if estimated_apart is None:
            return
        self.region_aparts.append(estimated_apart)
        self.pbar.update()
        remain_sec = round(
            (self.pbar.total - self.pbar.n) / self.pbar.format_dict["rate"] if
            self.pbar.format_dict["rate"] and self.pbar.total else 0, 2)
        td = datetime.timedelta(seconds=remain_sec)

        progress = round(self.pbar.n / self.flats_num * 100, 2)
        wait_minutes = int(td.total_seconds() // 60)
        wait_seconds = int(td.total_seconds() % 60)
        loading_info = f"{progress}%"

        loading_info += " Примерное время ожидания: "
        if wait_minutes != 0:
            loading_info += str(wait_minutes) + " мин."
        else:
            loading_info += str(wait_seconds) + " сек."
        loading_info += f" обработаные квартиры: {self.pbar.n}/{self.flats_num}"
        kwargs = dict(loading_inf=loading_info)
        if self.pbar.n > 1:
            kwargs["edit"] = True
            kwargs["progress_message_id"] = self.progress_message_id
        print("gswfsd")

        # time.sleep(15)
        self.progress_message_id = asyncio.run(send_loading_info(chat_id=self.chat_id, **kwargs))
        asyncio.run(send_apart_info(estimated_apart, None, chat_id=self.chat_id))

    def on_end_parse_region(self, region_name: str = None):
        self.region_aparts.sort(key=lambda apart: apart.rating, reverse=True)
        # for apart in self.region_aparts:
        #     asyncio.run(send_apart_info(apart, region_name, chat_id=self.chat_id))
        #     sleep(15)

    def check_unic(self, flat_id):
        return True

    def on_screenshot(self, screenshot, mess=None):
        asyncio.run(send_screenshot(screenshot, mess, chat_id=self.chat_id))
