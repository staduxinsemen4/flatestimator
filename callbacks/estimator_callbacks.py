import time

from typing import Union

from analytics import FlatEstimator

from callbacks.base_callbacks import BaseCallbacks
from objects import Flat
from utils.functions import get_bgr_image_from_url
from random import uniform
from multiprocessing.dummy import Pool
from itertools import repeat


class EstimatorCallbacks(BaseCallbacks):
    def __init__(self, flat_estimator: FlatEstimator, image_download_delay: Union[int, float] = 0):
        super().__init__(image_download_delay)
        # self.api_addresser = api_addresser
        # self.requests_session = requests.Session()
        self.flat_estimator = flat_estimator
        self.download_pool = Pool(10)

    def on_parse_flat(self, flat: Flat, user_agent: str, region_name: str, use_pool: bool = False):
        super(EstimatorCallbacks, self).on_parse_flat(flat, user_agent, region_name)
        if use_pool:
            res = zip(*self.download_pool.starmap(get_bgr_image_from_url,
                                                  zip(flat.images, repeat({'User-Agent': user_agent}))))
            imgs, bytes_imgs = res
        else:
            imgs = []
            bytes_imgs = []
            for img_url in flat.images:
                image, bytes_im = get_bgr_image_from_url(img_url, {'User-Agent': user_agent})
                imgs.append(image)
                bytes_imgs.append(bytes_im)
                print("downloaded image", img_url)
                if self.image_download_delay != 0.0:
                    time.sleep(.3)

        estimated_apart = self.flat_estimator.estimate(flat, imgs, bytes_imgs)

        return estimated_apart

    def on_end_parse_page_iteration(self, page_n: int):
        super(EstimatorCallbacks, self).on_end_parse_page_iteration(page_n)
