import os
import time

from callbacks.base_callbacks import BaseCallbacks
from objects import Flat
from utils.functions import download_photo
from multiprocessing.dummy import Pool
from itertools import repeat


class ParserCallbacks(BaseCallbacks):
    def __init__(self, output_folder: str, root_folder, image_download_delay: int = -1,
                 image_download_limit=-1):
        super().__init__(image_download_delay)
        self.output_folder = output_folder
        self.root_folder = root_folder
        self.image_download_limit = image_download_limit
        # self.download_pool = Pool(10)

    def on_parse_flat(self, flat: Flat, user_agent: str, region_name: str, out_dir: str):
        super(ParserCallbacks, self).on_parse_flat(flat, user_agent, region_name)
        im_folder = os.path.join(out_dir, str(flat.platform_id))

        im_out_pathes = [os.path.join(out_dir, str(flat.platform_id), 'images', f"{flat.platform_id}_{im_index}.jpg")
                         for
                         im_index in
                         range(len(flat.images))]
        with Pool(10) as download_pool:
            download_pool.starmap(download_photo,
                                            zip(flat.images, repeat({'User-Agent': user_agent}), im_out_pathes))
        # for n, im_url in enumerate(imgs_urls):
        #     if self.image_download_limit != -1 and (len(os.listdir(self.output_folder)) == self.image_download_limit):
        #         return False
        #     im_folder = os.path.join(self.output_folder, flat.platform_id)
        #     im_download_path = os.path.join(im_folder, "images", f"{flat.platform_id}_{n}.jpg")
        #     download_photo(im_url, im_download_path)
        #     flat.to_json_file(os.path.join(im_folder, flat.platform_id))
        #     time.sleep(self.image_download_delay)
        # return True

    def check_unic(self, flat_id):
        flat_id = str(flat_id)
        list_root_dirs = os.listdir(self.root_folder)
        if flat_id in list_root_dirs:
            return False
        for flat_cls_name in list_root_dirs:
            imgs_path = os.path.join(self.root_folder, flat_cls_name)
            for im_name in os.listdir(imgs_path):
                if flat_id in im_name:
                    print(f"{flat_id} найден в {imgs_path}")
                    return False
        return True
