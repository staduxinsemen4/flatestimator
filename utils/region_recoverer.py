from selenium.webdriver import ActionChains
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from ultralytics import YOLO
from undetected_chromedriver import Chrome
import numpy as np
import cv2
import time
import json
from orm_models import ParsingRegion
from urllib.parse import urlparse, parse_qs

from utils.functions import add_url_params


class RegionRecoverer:
    def __init__(self):
        self.model = YOLO("region/yolov8s_seg_region_2560_192/weights/best.pt")

    @staticmethod
    def generate_url_with_map_position_by_zoom(webdriver):
        print(webdriver.current_url)
        webdriver.find_element(By.XPATH, "//button[@data-marker='map-zoom-button_in']").click()
        time.sleep(3)
        print(webdriver.current_url)
        webdriver.find_element(By.XPATH, "//button[@data-marker='map-zoom-button_out']").click()
        time.sleep(3)
        print(webdriver.current_url)
        parsed_url = urlparse(webdriver.current_url)

        return webdriver.current_url, parse_qs(parsed_url.query).get("map")[0]

    def get_region_segments(self, webdriver) -> dict | None:
        screen_array = np.frombuffer(webdriver.get_screenshot_as_png(), np.uint8)
        screen_im = cv2.imdecode(screen_array, cv2.IMREAD_COLOR)
        print(screen_im.shape, "shape")
        predicted_polygon = self.model.predict(screen_im, retina_masks=True, show=False, conf=0.4, imgsz=(1376, 2560))[
            0]
        if len(predicted_polygon) == 0:
            return None
        predicted_polygon_segments = json.loads(predicted_polygon.tojson())[0]['segments']
        return predicted_polygon_segments

    def region_remember(self, webdriver, region: ParsingRegion) -> bool:
        region_url, map_position = self.generate_url_with_map_position_by_zoom(webdriver)
        predicted_polygon_segments = self.get_region_segments(webdriver)
        if predicted_polygon_segments is None:
            print("Запомнить регион не получилось")
            return False
        region.polygonX = predicted_polygon_segments['x']
        region.polygonY = predicted_polygon_segments['y']
        region.map_position = map_position
        region.url = region_url
        region.save()
        print(region.map_position, "map postion")
        return True

    @staticmethod
    def region_restore(webdriver, region: ParsingRegion):
        filters_elem = webdriver.find_element(By.XPATH, "//span[contains(@class,'button-filters-icon')]")
        start_loc = filters_elem.location
        draw_btn = webdriver.find_element(By.XPATH, "//div[contains(@class,'main-drawButton')]")

        for i in range(2):
            draw_btn.click()
            time.sleep(5)

        action_str = "ActionChains(webdriver, duration=0)"
        for i, (x, y) in enumerate(zip(region.polygonX, region.polygonY), start=1):
            curr_action_str = f".move_to_element_with_offset(filters_elem,xoffset={int(x) - start_loc['x'] - 10},yoffset={int(y) - start_loc['y'] - 10})"
            if i == 1:
                curr_action_str += ".click_and_hold()"
            action_str += curr_action_str
        action_str += ".release().perform()"
        eval(action_str)
        time.sleep(5)
        # parsed_url = urlparse(webdriver.current_url)
        new_url = add_url_params(webdriver.current_url, dict(map=region.map_position))
        region.url = new_url
        region.save()
        return new_url

    def cache_or_restore_region(self, webdriver: Chrome, region: ParsingRegion) -> tuple[str, str]:

        region_info = f"регион {region.name} id: {region.id}"
        print("Проверяю " + region_info)
        time.sleep(5)
        self.generate_url_with_map_position_by_zoom(webdriver)
        if region.map_position is None:
            print(f"Запоминаю " + region_info)
            result = self.region_remember(webdriver, region)
            if not result:
                return '', (
                    "Не получилось увидеть выделенную область на карте, пожалуйста измените ссылку в регионе, прислав ссылку на карту, в которой будет различима выделенная область, если нужно, область можно приблизить\n\n"
                    "Если в вашем регионе область различима, но вы видите эту ошибку, напишите об этом пожалуйста в техподдержку")
            return '', ''
        predicted_polygon_segments = self.get_region_segments(webdriver)

        if predicted_polygon_segments is None:
            print("Восстанавливаю " + region_info)
            new_url = self.region_restore(webdriver, region)
            print("Восстановлен " + region_info + f" url:{new_url}")
            return new_url, ''
        return '', ''
    # print(action_str)
