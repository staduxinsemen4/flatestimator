from selenium.webdriver import ActionChains
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from ultralytics import YOLO

from selenium.webdriver.common.by import By
import time

import cv2
import numpy as np
import json
import random


class CaptchaSolver:
    def __init__(self):
        self.captcha_model = YOLO(
            "model/yolov8s_captcha_200_/weights/best.pt")

    def check_captcha(self, webdriver):

        try:
            captcha_btn = WebDriverWait(webdriver, 5).until(
                # EC.visibility_of_all_elements_located()
                EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'form-action')]/button"))
            )
            return captcha_btn
        except:
            pass

        try:
            captcha_btn = WebDriverWait(webdriver, 5).until(
                # EC.visibility_of_all_elements_located()
                EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'geetest_btn_click')]"))
            )
            return captcha_btn
        except:
            return

    def solve_all(self, webdriver):
        captcha_btn = self.check_captcha(webdriver)
        if not captcha_btn:
            return False, -1
        for i in range(1500):
            print(f"Начинаю решать капчу номер {i + 1}")
            self.solve(captcha_btn, webdriver)
            captcha_btn = self.check_captcha(webdriver)
            if not captcha_btn:
                print("Вся капча решена")
                return True, i + 1

    def solve(self, captcha_btn, webdriver):

        captcha_btn.click()
        WebDriverWait(webdriver, 15).until(
            # EC.visibility_of_all_elements_located()
            EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'geetest_slice')]"))
        )
        for i in range(5):
            time.sleep(5)

            try:
                captcha_im = WebDriverWait(webdriver, 3).until(
                    # EC.visibility_of_all_elements_located()
                    EC.element_to_be_clickable((By.XPATH, "//div[contains(@class,'geetest_window')]"))
                )
            except:
                print("Капча решена")
                return True

            print(f"{i + 1}/5 попытка решить капчу")
            np_array = np.frombuffer(captcha_im.screenshot_as_png, np.uint8)
            image = cv2.imdecode(np_array, cv2.IMREAD_COLOR)
            res = self.captcha_model.predict(image, show=False, verbose=False, imgsz=200)
            json_results = json.loads(res[0].tojson())
            print(json_results)
            try:
                centre_x = self.get_centre_x(json_results[0].get('box'))
            except:
                centre_x = 150
            print(centre_x, 'centre_x')

            # cv2.imshow('res', image)*

            arrow_btn = webdriver.find_element(By.XPATH, "//div[contains(@class,'geetest_arrow')]")
            ActionChains(webdriver, duration=random.randint(149, 564)) \
                .click_and_hold(arrow_btn) \
                .move_by_offset(xoffset=int(centre_x) - 40, yoffset=0) \
                .release() \
                .perform()
        return False

    @staticmethod
    def get_centre_x(box):
        return box.get('x1') + (box.get('x2') - box.get('x1')) // 2
