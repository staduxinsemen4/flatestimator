from playhouse.migrate import *
from playhouse.postgres_ext import ArrayField

from orm_models import db, User, ParsingRegion
from peewee import ForeignKeyField
from datetime import datetime

migrator = PostgresqlMigrator(db)

field = DateTimeField(null=True)

migrate(
    # migrator.add_constraint()
    migrator.add_column('user', 'subscribe_datetime_end', field),
    # migrator.alter_column_type('denied_flats', 'tg_user_id', field),

)
