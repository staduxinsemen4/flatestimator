import os
import re
import urllib.request
import zipfile

import requests as req
from bs4 import BeautifulSoup
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait
from win32api import GetFileVersionInfo, HIWORD

from utils.functions import suppress_stdout_stderr


class SeleniumLoader:

    def __init__(self, chrome_path: str, chromedrivers_path: str, chromedriver_download_src: str, phantomjs_path: str):
        super(SeleniumLoader, self).__init__()
        self.chrome_path = chrome_path
        self.chromedrivers_path = chromedrivers_path
        self.chromedriver_download_src = chromedriver_download_src
        self.phantomjs_path = phantomjs_path

    def _check_chrome(self):
        chrome_version = ''
        if os.path.exists(self.chrome_path):
            chrome_version = str(HIWORD(GetFileVersionInfo(self.chrome_path, "\\")['FileVersionMS']))
        return chrome_version

    def _check_chromedriver(self, chrome_version):
        os.makedirs(self.chromedrivers_path, exist_ok=True)
        for chromedriver in os.listdir(self.chromedrivers_path):
            if re.findall(r'\d+', chromedriver)[0] == chrome_version:
                return os.path.join(self.chromedrivers_path, chromedriver)
        return ''

    def check_and_download(self):
        chrome_version = self._check_chrome()
        if not chrome_version:
            return ''
        driver_path = self._check_chromedriver(chrome_version)
        if not driver_path:
            driver_path = self._parse_chromedriver(chrome_version)
        return driver_path

    def _parse_chromedriver(self, chrome_version):
        resp = req.get(self.chromedriver_download_src)
        soup = BeautifulSoup(resp.text, 'lxml')

        for a in soup.find_all("a",class_=lambda class_: class_ and "XqQF9c" in class_,
                               text=lambda text: text and "ChromeDriver" in text):
            try:
                if not "index.html" in a["href"]: continue
            except:
                continue
            version = a.text.split(" ")[1].split(".")[0]
            if version == chrome_version:
                return self._download_chromedriver(a["href"], version)

    def _download_chromedriver(self, href, version):
        print(self.phantomjs_path)
        with suppress_stdout_stderr():
            webdriver_ = webdriver.PhantomJS(executable_path=self.phantomjs_path, service_log_path=os.path.devnull)

        webdriver_.get(href)

        WebDriverWait(webdriver_, 5).until(
            EC.presence_of_element_located((By.LINK_TEXT, "chromedriver_win32.zip"))
        )

        webdriver_href = webdriver_.find_element_by_link_text("chromedriver_win32.zip").get_attribute("href")

        zip_path = os.path.join(self.chromedrivers_path, "chromedriver_win32.zip")
        urllib.request.urlretrieve(webdriver_href, zip_path)

        with zipfile.ZipFile(zip_path, 'r') as zip_ref:
            zip_ref.extractall(self.chromedrivers_path)

        os.remove(zip_path)
        chromedriver_path = os.path.join(self.chromedrivers_path, f"chromedriver_{version}.exe")
        os.rename(os.path.join(self.chromedrivers_path, "chromedriver.exe"), chromedriver_path)

        webdriver_.quit()

        return chromedriver_path
