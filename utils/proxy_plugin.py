import os


def proxies_plug(username, password, endpoint, port, plugin_path: str):
    manifest_json = """
    {
         "version": "1.0.0",
         "manifest_version": 3,
         "name": "Chrome Proxy",
         "permissions": [
         "proxy",
         "tabs",
         "storage",
         "webRequest",
         "webRequestAuthProvider"
         ],
         "host_permissions": [
         "<all_urls>"
         ],
         "background": {
         "service_worker": "background.js"
         },
         "minimum_chrome_version": "22.0.0"
}
    """

    background_js = """
    var config = {
            mode: "fixed_servers",
            rules: {
              singleProxy: {
                scheme: "http",
                host: "%s",
                port: parseInt(%s)
              },
              bypassList: ["localhost"]
            }
          };

    chrome.proxy.settings.set({value: config, scope: "regular"}, function() {});

    function callbackFn(details) {
        return {
            authCredentials: {
                username: "%s",
                password: "%s"
            }
        };
    }

    chrome.webRequest.onAuthRequired.addListener(
                callbackFn,
                {urls: ["<all_urls>"]},
                ['blocking']
    );
    """ % (endpoint, port, username, password)
    os.makedirs(plugin_path, exist_ok=True)
    with open(f"{plugin_path}/manifest.json", "w") as f:
        f.write(manifest_json)
    with open(f"{plugin_path}/background.js", "w") as f:
        f.write(background_js)
