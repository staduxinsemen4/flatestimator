import multiprocessing
import os
import re
import sys
import time
import traceback
import urllib.request
import config
import cv2
import numpy as np
import requests


# from tensorflow.keras.models import Model
# from tensorflow.python.keras.layers import deserialize, serialize
# from tensorflow.python.keras.saving import saving_utils


class suppress_stdout_stderr(object):
    def __enter__(self):
        self.outnull_file = open(os.devnull, 'w')
        self.errnull_file = open(os.devnull, 'w')

        self.old_stdout_fileno_undup = sys.stdout.fileno()
        self.old_stderr_fileno_undup = sys.stderr.fileno()

        self.old_stdout_fileno = os.dup(sys.stdout.fileno())
        self.old_stderr_fileno = os.dup(sys.stderr.fileno())

        self.old_stdout = sys.stdout
        self.old_stderr = sys.stderr

        os.dup2(self.outnull_file.fileno(), self.old_stdout_fileno_undup)
        os.dup2(self.errnull_file.fileno(), self.old_stderr_fileno_undup)

        sys.stdout = self.outnull_file
        sys.stderr = self.errnull_file
        return self

    def __exit__(self, *_):
        sys.stdout = self.old_stdout
        sys.stderr = self.old_stderr

        os.dup2(self.old_stdout_fileno, self.old_stdout_fileno_undup)
        os.dup2(self.old_stderr_fileno, self.old_stderr_fileno_undup)

        os.close(self.old_stdout_fileno)
        os.close(self.old_stderr_fileno)

        self.outnull_file.close()
        self.errnull_file.close()


def log_error_to_file(logger):
    err_message = f"from process: {multiprocessing.current_process().name}: {traceback.format_exc()}"
    logger.error(err_message)


def url_to_resp(image_url: str, headers: dict):
    max_try = 4
    resp = None
    req = urllib.request.Request(
        image_url,
        data=None,
        headers=headers

    )
    for i in range(max_try):
        try:
            resp = urllib.request.urlopen(req, timeout=5)
            return resp
        except:
            print(traceback.format_exc())
            print(f"Изображение не загружено {i + 1} попытка")
            time.sleep(3)
            continue
    if not resp:
        print("Изображение не прочитано")
        return


def get_bgr_image_from_url(image_url: str, headers: dict, only_bytes=False):
    resp = url_to_resp(image_url, headers)
    if not resp:
        return
    bytes_im = bytes(resp.read())
    if not only_bytes:
        image = np.frombuffer(bytes_im, dtype="uint8")
        image = cv2.imdecode(image, cv2.IMREAD_COLOR)
        return image, bytes_im
    else:
        return bytes_im


def prepare_image(bgr_image, shape):
    return cv2.resize(bgr_image, shape[:-1])


def get_target_value(res, target_idx):
    target_value = res[target_idx]
    res_value = int(round(target_value * 100))
    return res_value, target_value


def find_numbers(input_str) -> list:
    input_str = input_str.replace(" ", '')
    numbers = []
    for number in re.findall("[-+]?[.]?[\d]+(?:,\d\d\d)*[\.]?\d*(?:[eE][-+]?\d+)?",
                             input_str):
        try:
            number = int(number)
        except:
            number = float(number)
        numbers.append(number)
    return numbers


def download_photo(image_url: str, headers: dict, image_path_out: str):
    resp = url_to_resp(image_url, headers)
    if not resp:
        return
    img_data = bytes(resp.read())
    os.makedirs(os.path.dirname(image_path_out), exist_ok=True)

    with open(image_path_out, 'wb') as handler:
        handler.write(img_data)
    print(f"Изображение загружено: {image_path_out}\nFrom: {image_url}")
    return True


def init_dir_struct(out_dir):
    for dir_view_name in config.ROOMS_VIEWS:
        for dir_rating_name in config.RATINGS:
            os.makedirs(os.path.join(out_dir, dir_view_name, str(dir_rating_name)))


# def unpack(model, training_config, weights):
#     restored_model = deserialize(model)
#     if training_config is not None:
#         restored_model.compile(
#             **saving_utils.compile_args_from_training_config(
#                 training_config
#             )
#         )
#     restored_model.set_weights(weights)
#     return restored_model
#
#
# def make_keras_picklable():
#     def __reduce__(self):
#         model_metadata = saving_utils.model_metadata(self)
#         training_config = model_metadata.get("training_config", None)
#         model = serialize(self)
#         weights = self.get_weights()
#         return (unpack, (model, training_config, weights))
#
#     cls = Model
#     cls.__reduce__ = __reduce__


from urllib.parse import urlencode, unquote, urlparse, parse_qsl, ParseResult
from json import dumps


def add_url_params(url, params):
    """ Add GET params to provided URL being aware of existing.

    :param url: string of target URL
    :param params: dict containing requested params to be added
    :return: string with updated URL

    >> url = 'https://stackoverflow.com/test?answers=true'
    >> new_params = {'answers': False, 'data': ['some','values']}
    >> add_url_params(url, new_params)
    'https://stackoverflow.com/test?data=some&data=values&answers=false'
    """
    # Unquoting URL first so we don't lose existing args
    url = unquote(url)
    # Extracting url info
    parsed_url = urlparse(url)
    # Extracting URL arguments from parsed URL
    get_args = parsed_url.query
    # Converting URL arguments to dict
    parsed_get_args = dict(parse_qsl(get_args))
    # Merging URL arguments dict with new params
    parsed_get_args.update(params)

    # Bool and Dict values should be converted to json-friendly values
    # you may throw this part away if you don't like it :)
    parsed_get_args.update(
        {k: dumps(v) for k, v in parsed_get_args.items()
         if isinstance(v, (bool, dict))}
    )

    # Converting URL argument to proper query string
    encoded_get_args = urlencode(parsed_get_args, doseq=True)
    # Creating new parsed result object based on provided with new
    # URL arguments. Same thing happens inside urlparse.
    new_url = ParseResult(
        parsed_url.scheme, parsed_url.netloc, parsed_url.path,
        parsed_url.params, encoded_get_args, parsed_url.fragment
    ).geturl()

    return new_url


if __name__ == "__main__":
    init_dir_struct("D:\\datasets\\aparts")
