import multiprocessing
import os

import commentjson as json

from analytics import FlatEstimator
from analytics.models import FlatEstimatorModel
from api_addresser import ApiAddresser
from callbacks import ParserCallbacks, EstimatorCallbacks
from parsers import ApartmentsParserBase
from parsers.avito_parser import AvitoParser
from parsers.cian_parser import CianParser

parsers = {"avito": AvitoParser, "cian": CianParser}


def get_parser(output_url: str):
    parser_: ApartmentsParserBase
    for parser_name, parser_ in parsers.items():
        if parser_name in output_url:
            parser = parser_
            return parser
    return None


def start_flat_parser(data: dict, chromedriver_path: str, image_download_delay: int):
    callbacks = ParserCallbacks(data.get("output_folder"), image_download_delay)
    parser: ApartmentsParserBase
    parser = get_parser(data.get("output_url"))
    if not parser:
        return
    parser_inited = parser(chromedriver_path, callbacks, data.get("headless"))
    parser_inited.parse(data.get("output_url"), mode="parser")


def start_flat_estimator(data: dict, chromedriver_path: str, image_download_delay: int,
                         flat_network: FlatEstimatorModel,
                         api_addresser: ApiAddresser):
    flat_estimator = FlatEstimator(flat_network)
    callbacks = EstimatorCallbacks(flat_estimator, api_addresser, image_download_delay)
    parser: ApartmentsParserBase
    parser = get_parser(data.get("output_url"))
    if not parser:
        return
    parser_inited = parser(chromedriver_path, callbacks, data.get("headless"))
    parser_inited.parse(data.get("output_url"), mode="estimator")


def run_from_settings_files(settings_files_path: str, chromedriver_path: str, image_download_delay: int = 1,
                            flat_network=None, api_addresser=None):
    prc = []
    for settings_file in os.listdir(settings_files_path):
        with open(os.path.join(settings_files_path, settings_file), "r", encoding="utf8") as read_file:
            data = json.load(read_file)
            mode = data.get("mode", '')

            if not mode:
                print("mode is not selected, going to the next file")
                continue
            if not data.get("enabled", False):
                print("file is not enabled, going to the next file")
                continue
            if mode == "parser":
                target = start_flat_parser
                args = (chromedriver_path, image_download_delay)
            elif mode == "estimator":
                target = start_flat_estimator
                args = (chromedriver_path, image_download_delay, flat_network, api_addresser)
            else:
                print("no such mode exist")
                continue

            for data_for_run in data.get("data"):
                print((data_for_run, *args))
                p = multiprocessing.Process(target=target,
                                            args=(data_for_run, *args))

                p.start()
                prc.append(p)
    [p.join() for p in prc]
