import multiprocessing

from analytics.models import get_keras_mobilenet_model
from api_addresser import ApiAddresser
from api_config import api_url, email, password
from definitions import CHROME_PATH, CHROMEDRIVERS_PATH, CHROMEDRIVER_DOWNLOAD_SRC, PHANTOMJS_PATH, \
    ESTIMATOR_TASKS_PATH, ESTIMATOR_MODEL_PATH, ESTIMATOR_CLASSES_PATH, ESTIMATOR_MODEL_SHAPE, IMAGE_DOWNLOAD_DELAY
from utils.runner_from_settings import run_from_settings_files
from utils.selenium_loader import SeleniumLoader

if __name__ == "__main__":
    multiprocessing.freeze_support()
    loader = SeleniumLoader(chrome_path=CHROME_PATH, chromedrivers_path=CHROMEDRIVERS_PATH,
                            chromedriver_download_src=CHROMEDRIVER_DOWNLOAD_SRC,
                            phantomjs_path=PHANTOMJS_PATH)
    chromedriver_path = loader.check_and_download()

    model = get_keras_mobilenet_model(model_path=ESTIMATOR_MODEL_PATH,
                                      classes_path=ESTIMATOR_CLASSES_PATH,
                                      shape=ESTIMATOR_MODEL_SHAPE,
                                      )
    api_addresser = ApiAddresser(api_url=api_url, email=email, password=password)
    run_from_settings_files(ESTIMATOR_TASKS_PATH, chromedriver_path, IMAGE_DOWNLOAD_DELAY, model, api_addresser)
