import base64

from telegram import InputMediaPhoto, InlineKeyboardMarkup, InlineKeyboardButton, Update
import sys, os
from telegram.constants import ParseMode
from telegram.ext import ContextTypes
import threading
from telegram_bot import application, bot, params, admin_chat_id
from orm_models import DeniedFlat, db
from celery_worker.utils import remove_tasks
from parsers.avito_parser import DealType
from utils.functions import get_bgr_image_from_url
from fake_useragent import UserAgent
from uuid import uuid4

RUSSIFY = {"agency": "через агентство", "private": 'от собственника', 'realtor': "через риелтора"}
RUSSIFY_DEAL = {DealType.DAILY: "в день", DealType.RENT: "в месяц", DealType.BUY: ''}
RUSSIFY_DEAL_FROM = {DealType.DAILY: "Сдается", DealType.RENT: "Сдается", DealType.BUY: 'Продается'}

user_agent_randomizer: UserAgent = UserAgent(platforms='pc')


async def send_loading_info(loading_inf, edit=False, progress_message_id=None, chat_id=None):
    async with bot:
        if not edit:
            msg = await bot.send_message(chat_id=chat_id,
                                         text=loading_inf,
                                         **params)



        else:
            msg = await bot.edit_message_text(chat_id=chat_id, text=loading_inf,
                                              **params, message_id=progress_message_id)
        return msg.id


async def send_screenshot(screenshot, mess, chat_id=None):
    img = base64.b64decode(screenshot)
    async with bot:
        await bot.send_photo(chat_id=chat_id, photo=img, caption=mess, parse_mode=ParseMode.HTML)


async def send_apart_info_start_message(chat_id):
    async with bot:
        await bot.send_message(chat_id=chat_id, text="Проверил актуальные квартиры и нашел лучшие варианты:", **params)


async def send_apart_info(estimated_apart, aparts_num: int, apart_num: int, region_name: str = None, chat_id=None,
                          region_id: int = None):
    if not estimated_apart['estimated_photos']:
        return
    apart_rating = round(estimated_apart['rating'], 2)
    async with bot:
        sorted_est_photos = sorted(estimated_apart['estimated_photos'], key=lambda photo: photo['rating'])

        if len(sorted_est_photos) <= 10:
            images_to_send = sorted_est_photos
        else:
            images_to_send = sorted_est_photos[-5:]
            images_to_send.extend(sorted_est_photos[:5])
        print(images_to_send)
        flat_info = estimated_apart['flat_info']
        apart_rating_rounded = round(apart_rating)

        rating_mess = "★" * apart_rating_rounded + "☆" * (5 - apart_rating_rounded)
        rooms_number = flat_info['rooms_number']
        if flat_info['deal_type'] != DealType.BUY:

            deposit_place_text = f"<b>Залог</b>: {flat_info['deposit']:,}₽\n"
        else:
            deposit_place_text = f"<b>Стоимость за м²</b>: {flat_info['meter_cost']:,}₽\n"

        if flat_info['deal_type'] == DealType.RENT:
            commission_place_text = f"<b>Комиссия</b>: {flat_info['commission']:,}₽\n"
        else:
            commission_place_text = ''
        mess = (f"<a href='{estimated_apart['flat_info']['link']}'>Источник: Авито</a>\n"
                f"<b>Регион</b>: #{region_name} (осталось {aparts_num - apart_num})\n"
                f"<b>Оценка</b>: {rating_mess} ({apart_rating}/5)\n"
                f"<b>Цена</b>: {flat_info['price']:,}₽ "
                f"{RUSSIFY_DEAL.get(flat_info['deal_type'])}" + "\n"
                                                                f"{commission_place_text}"
                                                                f"{deposit_place_text}"
                                                                f"{RUSSIFY_DEAL_FROM.get(flat_info['deal_type'])} {RUSSIFY.get(flat_info['owner'])}\n"
                                                                f"<b>Общая площадь</b>: {round(flat_info['house_area'])}м²\n"
                                                                f"<b>Количество комнат</b>: {rooms_number if rooms_number > 0 else '0 (Студия)'}\n"
                                                                f"<b>Этаж</b>: {flat_info['floor']} из {flat_info['max_floor']}\n"
                                                                f"<b>Адрес</b>: {flat_info['address']}")
        media = [
            InputMediaPhoto(
                get_bgr_image_from_url(estimated_photo["url"], only_bytes=True,
                                       headers={'User-Agent': user_agent_randomizer.random}),
                caption=f"Рейтинг фото: {round(estimated_photo['rating'], 2)}" if chat_id == admin_chat_id else None,
                filename=f"{uuid4()}.jpg", )
            for estimated_photo in
            images_to_send]
        print(media)
        media_msg = await bot.sendMediaGroup(
            media=media,
            chat_id=chat_id,
            # caption=f"<a href='{estimated_apart['flat_info']['link']}'>Источник: Авито</a>",
            parse_mode=ParseMode.HTML, **params)

        callback_data = f"delete_apart:{estimated_apart['flat_info']['platform_id']}:{region_id}:{estimated_apart['flat_info']['platform']}:{media_msg[0].message_id}:{len(images_to_send)}"

        markup = InlineKeyboardMarkup(
            [[InlineKeyboardButton(f"Удалить",
                                   callback_data=callback_data),
              InlineKeyboardButton(f"Открыть объявление", url=estimated_apart['flat_info']['link'])],
             [InlineKeyboardButton("Главное меню", callback_data="main_menu_apart")]])
        await bot.send_message(chat_id=chat_id, text=mess,
                               reply_markup=markup, parse_mode=ParseMode.HTML, disable_web_page_preview=True, **params)


async def delete_apart(update: Update, context: ContextTypes.DEFAULT_TYPE):
    delete_info = update.callback_query.data.split(':')

    apart_id, region_id, apart_platform = delete_info[1], delete_info[2], delete_info[3]
    start_media_msg_id = int(delete_info[4])
    len_media_msg_id = delete_info[5]
    user_id = update.effective_message.chat_id
    currents_msg_id = update.effective_message.message_id
    msgs_to_delete = [int(currents_msg_id)]
    msgs_to_delete.extend([start_media_msg_id + add for add in range(int(len_media_msg_id))])

    await bot.deleteMessages(user_id, msgs_to_delete)

    DeniedFlat.create(tg_user_id=user_id, platform_id=apart_id, platform=apart_platform, region_id=region_id)


async def send_region_err_from_task(chat_id, err_msg: str):
    markup = InlineKeyboardMarkup([[InlineKeyboardButton("Главное меню", callback_data="main_menu_apart")]])
    async with bot:
        await bot.send_message(chat_id, err_msg, reply_markup=markup)
