import sys
sys.path.append('.')
from telegram_bot.conversation import conv_handler
from telegram_bot.bot_sender import delete_apart
from telegram_bot import application
from telegram.ext import CallbackQueryHandler
from telegram import Update


if __name__ == '__main__':
    application.add_handler(conv_handler)
    application.add_handler(CallbackQueryHandler(delete_apart, pattern=lambda c: c.startswith('delete_apart:')))
    application.run_polling(allowed_updates=Update.ALL_TYPES)
