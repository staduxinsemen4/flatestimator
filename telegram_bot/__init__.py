from telegram.ext import ApplicationBuilder, AIORateLimiter, PicklePersistence, Defaults
import sys

df = Defaults(block=False)
sys.path.append('.')
persistence = PicklePersistence(filepath="local_data/telebot_data/flat_estimator.pickle")
application = ApplicationBuilder().token("7427205967:AAGPYVOYwyHp58PSqdgoM-9f7V5uK-_Gs1Y").read_timeout(
    30).pool_timeout(30).connect_timeout(30).write_timeout(30).defaults(df).persistence(
    persistence).build()
bot = application.bot

admin_chat_id = 469576140
support_chat_id = 7450938717
params = {"read_timeout": 30, "pool_timeout": 30, "connect_timeout": 30, "write_timeout": 30}
