import logging
from uuid import uuid4

from redbeat import RedBeatSchedulerEntry
from telegram import ReplyKeyboardRemove, Update, InlineKeyboardMarkup, InlineKeyboardButton
from telegram.constants import ParseMode
from telegram.ext import (
    CommandHandler,
    ContextTypes,
    ConversationHandler,

    MessageHandler,
    filters, CallbackQueryHandler
)
from datetime import datetime, timedelta

from yookassa import Payment

from celery_worker.utils import get_schedules, check_user_active_schedules, remove_tasks
from config import CassaConfig
from orm_models import StartedTasks, db, ParsingRegion, SendedFlat, DeniedFlat, User
from telegram_bot import application, admin_chat_id, support_chat_id
from telegram_bot.bot_utils import send_tutorial, run_parsing_entry, run_stop_parsing_proc, is_subscribe_alive, \
    reset_sended_aparts
from telegram_bot.validators import validate_region_url, validate_region_name, validators_map, \
    validate_regions_to_create, validate_regions_to_run
from celery_worker.parser_worker import celery_parser
from redbeat.schedules import rrule
import threading
from orm_utils import get_updated_user

# Enable logging
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.INFO
)
# set higher logging level for httpx to avoid all GET and POST requests being logged
logging.getLogger("httpx").setLevel(logging.WARNING)

logger = logging.getLogger(__name__)

(ON_MAIN_MENU_ACTIONS, ON_REGION_INFO_ACTION,
 CHANGE_SENT, ON_REGIONS_LIST_ACTIONS, ON_BEGIN_REGION_CREATION_EVENT,
 FINAL_STEP_REGION_CREATION, TYPING_CHOICE,
 ON_STARTED_PARSING_ACTION, ON_GET_SUB_INFO_ACTION, ON_GET_PAYMENT_INFO_ACTION, ON_BEGIN_START_PARSING_ADMIN,
 ON_BEGIN_STOP_PARSING_ADMIN) = range(
    12)
RUSSIFY_CHANGE = {"name": "Отправьте новое имя региона",
                  "url": "Отправьте новую ссылку на регион"}


def get_russify_change(region_name: str, region_current_name: str, current_changing_key):
    RUSSIFY_POST_CHANGE = {"name": f"Имя региона #{region_name} было изменено на #{region_current_name}",
                           "url": f"Ссылка региона #{region_name} была изменена"}
    return RUSSIFY_POST_CHANGE.get(current_changing_key)


send_args = dict(disable_web_page_preview=True, parse_mode=ParseMode.HTML)


async def main_menu(update: Update, context: ContextTypes.DEFAULT_TYPE, parsing_stopped=False, send_new=False):
    if update.callback_query:
        if "main_menu_payment:" in update.callback_query.data:
            await check_payment(update, context)
        await update.callback_query.answer()
    user = get_updated_user(update.effective_user)

    subscribe_alive, subscribe_time_left = is_subscribe_alive(user)
    subscribe_text = "Статус подписки: "
    if not subscribe_alive:
        subscribe_text += "<b>Подписка закончилась</b>"
    else:
        subscribe_text += f"<b>Осталось {subscribe_time_left.days} д. {subscribe_time_left.seconds // 3600} ч.</b>"

    is_active_shedules = check_user_active_schedules(update.effective_user.id)
    menu_text = ("Главное меню\n"
                 f"Статус поиска квартир: <b>{'Включен' if is_active_shedules and not parsing_stopped else 'Выключен'}</b>\n"
                 f"{subscribe_text}\n"
                 f"<a href='https://t.me/RealtyRenterBotSupport'>Техподдержка по вопросам и предложениям</a>")
    search_btn_text, search_btn_callback = (
        "Запустить поиск квартир", "start_parsing") if not is_active_shedules or parsing_stopped else (
        "Остановить поиск квартир", "stop_parsing")
    buttons = [[InlineKeyboardButton(f"Список регионов", callback_data="regions_list"),
                InlineKeyboardButton(f"Добавить регион", callback_data="new_region_main")],
               [InlineKeyboardButton(search_btn_text, callback_data=search_btn_callback)],
               [InlineKeyboardButton("Купить/продлить подписку", callback_data="get_sub")]]

    if update.effective_user.id == admin_chat_id:
        buttons.append([InlineKeyboardButton("(Admin) Остановить поиск", callback_data="begin_stop_parsing_admin")])
        buttons.append([InlineKeyboardButton("(Admin) Запустить поиск", callback_data="begin_start_parsing_admin")])
    markup = InlineKeyboardMarkup(
        buttons)
    if update.callback_query:
        if update.callback_query.data == "main_menu_apart" or send_new:
            await application.bot.send_message(update.effective_chat.id, menu_text, reply_markup=markup,
                                               **send_args)
        else:

            await update.callback_query.edit_message_text(menu_text, reply_markup=markup, **send_args)
    elif update.message.text == "/start":
        await update.message.reply_text(
            "Привет, смотри обучающее видео снизу, для того что бы начать пользоваться ботом и получать лучшие варианты квартир!\n\n"
            "<i>* Кстати, создавать регионы и настраивать фильтры удобнее с компьютера</i>",
            reply_markup=ReplyKeyboardRemove(), **send_args)
        await send_tutorial(update, context)
        await update.message.reply_text(
            "Тебе будут доступны 2 дня бесплатной подписки с лимитом созданных регионов в 1шт. Создавай свой первый регион и запускай поиск!\n\n"
            "<i>* Подписка дает возможность пользоваться ботом, оперативно получая лучшую недвижимость и создавать одновременно до 5 регионов</i>",
            reply_markup=ReplyKeyboardRemove(), **send_args)
        await update.message.reply_text(menu_text, reply_markup=markup, **send_args)
    else:
        await update.message.reply_text(menu_text, reply_markup=markup, **send_args)

    return ON_MAIN_MENU_ACTIONS


async def get_sub_info(update: Update, context: ContextTypes.DEFAULT_TYPE, text: str = None):
    if "back:" in update.callback_query.data:
        await check_payment(update, context)
    if update.callback_query:
        await update.callback_query.answer()
    markup = InlineKeyboardMarkup(
        [[InlineKeyboardButton("Купить 7 дней подписки за 190₽", callback_data=f"pay_info:7:190")],
         [InlineKeyboardButton("Купить 30 дней подписки за 490₽", callback_data=f"pay_info:30:490")],
         [InlineKeyboardButton("Отмена", callback_data=f"main_menu")]])
    text = text if text is not None else "Здесь вы можете приобрести подписку или продлить текущую.\n\n" \
                                         "<i>* Подписка дает возможность пользоваться ботом, оперативно получая лучшую недвижимость и создавать одновременно до 5 регионов</i>"
    if update.callback_query:
        await update.callback_query.edit_message_text(
            text,
            reply_markup=markup, **send_args)
    else:
        await update.effective_user.send_message(text,
                                                 reply_markup=markup, **send_args)
    return ON_GET_SUB_INFO_ACTION


async def get_payment_info(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.callback_query.answer()
    days_num, sum = update.callback_query.data.split(":")[1:]
    days_num = int(days_num)
    sum = int(sum)
    idempotency_key = uuid4()
    builder = CassaConfig.payment_builder
    builder["description"] = (f"TG_id: {update.effective_user.id}\n"
                              f"Срок подписки: {days_num} дней\n")
    builder["amount"]["value"] = str(sum)
    item = builder["receipt"]["items"][0]
    item["description"] = f"Покупка подписки на telegram бота Renter сроком {days_num} д."
    item["amount"]["value"] = str(sum)
    print(builder, 'bweqawe')
    payment = Payment.create(CassaConfig.payment_builder, idempotency_key=idempotency_key)
    payment_url = payment.confirmation.confirmation_url
    payment_id = payment.id
    markup = InlineKeyboardMarkup([[InlineKeyboardButton("Оплатить", url=payment_url)],
                                   [InlineKeyboardButton("Проверить оплату",
                                                         callback_data=f"check_payment:{payment_id}:{days_num}:{sum}")],
                                   [InlineKeyboardButton("Назад", callback_data=f"back:{payment_id}:{days_num}:{sum}"),
                                    InlineKeyboardButton("В главное меню",
                                                         callback_data=f"main_menu_payment:{payment_id}:{days_num}:{sum}")]])
    await update.callback_query.edit_message_text(
        f"Вы покупаете {days_num} дней подписки за {sum}₽",
        reply_markup=markup, **send_args)
    return ON_GET_PAYMENT_INFO_ACTION


async def check_payment(update: Update, context: ContextTypes.DEFAULT_TYPE):
    payment_id, days_num, sum = update.callback_query.data.split(":")[1:]
    payment = Payment.find_one(payment_id)
    if payment.status == 'succeeded':
        user: User = User.get(tg_user_id=update.effective_user.id)
        print(user.subscribe_datetime_end, 'end')
        increase_td = timedelta(days=int(days_num), minutes=10)
        subscribe_alive, subscribe_time_left = is_subscribe_alive(user)
        if subscribe_alive:
            increase_td += subscribe_time_left
        utcnow = datetime.utcnow()
        user.subscribe_datetime_end = utcnow + increase_td
        user.is_subscriber = True
        user.last_subscribe_datetime = utcnow
        user.save()
        await update.effective_user.send_message("Оплата прошла успешно, приобретенные дни подписки зачислены",
                                                 )
        if "check_payment:" in update.callback_query.data:
            await update.callback_query.answer()
            return await main_menu(update, context)
    else:
        if "check_payment:" in update.callback_query.data:
            await update.effective_user.send_message("Оплата ещё не прошла, либо произошла ошибка")

    await update.callback_query.answer()


async def request_empty_region_creation(update: Update, context: ContextTypes.DEFAULT_TYPE, edited: bool = False,
                                        mess: str = ''):
    markup = InlineKeyboardMarkup([[InlineKeyboardButton("Да", callback_data="new_region_main"),
                                    InlineKeyboardButton("Нет", callback_data="regions_list_back")]])
    mess = ("У вас нет ни одного региона\n"
            "Создать новый регион?") if not mess else mess
    if not update.callback_query:
        await application.bot.send_message(update.effective_chat.id, mess, reply_markup=markup)
    else:
        if edited:
            await application.bot.send_message(update.effective_chat.id, mess, reply_markup=markup)
        else:
            await update.callback_query.edit_message_text(mess, reply_markup=markup)


async def regions_list(update: Update, context: ContextTypes.DEFAULT_TYPE, edited: bool = False) -> int:
    regions = ParsingRegion.select().where(ParsingRegion.tg_user_id == update.effective_user.id).order_by(
        ParsingRegion.id.desc())
    if not regions:
        await request_empty_region_creation(update, context, edited)
        return ON_REGIONS_LIST_ACTIONS
    user: User = User.get(tg_user_id=update.effective_user.id)
    region_buttons = [
        [InlineKeyboardButton(text=region.name, callback_data=f"region:{region.id}")] for region in
        regions
    ]
    region_buttons.extend([
        [
            InlineKeyboardButton(f"Добавить регион", callback_data="new_region_list"),
            InlineKeyboardButton(f"В главное меню", callback_data=f"regions_list_back")

        ]
    ])
    markup = InlineKeyboardMarkup(
        region_buttons)

    region_count_txt = f"<b>{len(regions)} из {'5' if user.is_subscriber else '1'}</b>"
    if not user.is_subscriber:
        region_count_txt += " (5 регионов при купленной подписке)"
    mess = ("Текущие добавленные регионы\n"
            f"{region_count_txt}")
    if update.message:
        await update.message.reply_text(mess, reply_markup=markup, **send_args)
    else:
        await update.callback_query.answer()
        if edited:
            await application.bot.send_message(update.effective_chat.id, mess, reply_markup=markup, **send_args)
        else:
            await update.callback_query.edit_message_text(mess, reply_markup=markup, **send_args)

    return ON_REGIONS_LIST_ACTIONS


async def region_info(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    if update.callback_query:

        region_id = update.callback_query.data.split(':')[1]
    else:
        region_id = context.chat_data["current_changing"]["id"]
        del context.chat_data["current_changing"]

    region = ParsingRegion.get(ParsingRegion.id == region_id)

    print(region)
    active_region_text = "Выключить регион" if region.active else "Включить регион"
    markup = InlineKeyboardMarkup(
        [
            [InlineKeyboardButton(f"Открыть регион", url=region.url)],
            [InlineKeyboardButton(active_region_text, callback_data=f"change_active:{region.id}")],
            [InlineKeyboardButton(f"Изменить имя", callback_data=f"changing:name:{region.id}")],
            [InlineKeyboardButton(f"Изменить url", callback_data=f"changing:url:{region.id}")],
            [InlineKeyboardButton(f"Сбросить квартиры", callback_data=f"reset:{region.id}")],
            [InlineKeyboardButton(f"Удалить регион", callback_data=f"delete:{region.id}")],

            [
                InlineKeyboardButton(f"В список регионов", callback_data=f"region_info_back"),
                InlineKeyboardButton(f"В главное меню", callback_data=f"main_menu")
            ]
        ])
    region_text = (f"Регион #{region.name}\n"
                   f"Статус: {'Включен' if region.active else 'Выключен'}")

    if update.callback_query:
        await update.callback_query.edit_message_text(
            region_text, reply_markup=markup
        )
    else:
        await update.message.reply_text(region_text, reply_markup=markup)

    return ON_REGION_INFO_ACTION


async def reset_region_sended_aparts(update: Update, context: ContextTypes.DEFAULT_TYPE):
    region_id = update.callback_query.data.split(':')[1]
    region = ParsingRegion.get(ParsingRegion.id == region_id)

    reset_sended_aparts(region_id, region.tg_user_id)
    await update.callback_query.answer()
    await regions_list(update, context)
    srch_active = check_user_active_schedules(update.effective_user.id)
    txt = (
        f"В регионе #{region.name} сброшены все присланные/удаленные квартиры."
        f" В течении нескольких часов все квартиры из этого региона начнут приходить заново. Поиск уже запущен, перезапускать его не обязательно") if srch_active else \
        (
            f"В регионе #{region.name} сброшены все присланные/удаленные квартиры."
            f" В течении нескольких часов все квартиры из этого региона начнут приходить заново. Запустите поиск что бы начать получать квартиры из всех созданных регионов")
    await update.effective_user.send_message(
        txt)
    return ON_REGIONS_LIST_ACTIONS


async def begin_region_creation(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    await update.callback_query.answer()
    error = validate_regions_to_create(update.effective_user.id)
    if error:
        # await application.bot.send_message(update.effective_chat.id, error)
        # return await regions_list(update, context)
        await update.callback_query.edit_message_text(
            text=error
        )
        return await regions_list(update, context, edited=True)

    markup = InlineKeyboardMarkup([[InlineKeyboardButton("Отмена создания региона", callback_data=f"cancel_creation_" +
                                                                                                  update.callback_query.data.split(
                                                                                                      "_")[2])],
                                   [InlineKeyboardButton("Открыть авито аренда недвижимости",
                                                         url=f"https://www.avito.ru/moskva_i_mo/kvartiry/sdam/na_dlitelnyy_srok-ASgBAgICAkSSA8gQ8AeQUg?context=H4sIAAAAAAAA_0q0MrSqLraysFJKK8rPDUhMT1WyLrYytlLKTSxQsq4FBAAA__8Xe4TEHwAAAA&localPriority=0&map=eyJzZWFyY2hBcmVhIjp7ImxhdEJvdHRvbSI6NTUuNTAxMzkyNDUzMDk1NzU0LCJsYXRUb3AiOjU1Ljk1MDcxMzkxNzgzNTc4NSwibG9uTGVmdCI6MzYuOTk1NTI3MTAwNTYzNjA2LCJsb25SaWdodCI6MzguNDEwMDE2ODQ2NjU3MzM1fSwiem9vbSI6MTF9")],
                                   [InlineKeyboardButton("Открыть авито покупка недвижимости",
                                                         url=f"https://www.avito.ru/moskva_i_mo/kvartiry/prodam-ASgBAgICAUSSA8YQ?context=H4sIAAAAAAAA_0q0MrSqLraysFJKK8rPDUhMT1WyLrYytlLKTSxQsq4FBAAA__8Xe4TEHwAAAA&map=eyJzZWFyY2hBcmVhIjp7ImxhdEJvdHRvbSI6NTUuNTAxMzkyNDUzMDk1NzU0LCJsYXRUb3AiOjU1Ljk1MDcxMzkxNzgzNTc4NSwibG9uTGVmdCI6MzYuOTk1NTI3MTAwNTYzNjA2LCJsb25SaWdodCI6MzguNDEwMDE2ODQ2NjU3MzM1fSwiem9vbSI6MTF9")]
                                   ])
    await update.callback_query.edit_message_text(
        text=f"Давайте добавим новый регион для поиска квартир\n\n"
             f"Для этого:\n"
             f"1) Откройте ссылку на аренду или покупку недвижимости по кнопкам снизу\n"
             f"2) Выберите город\n"
             f"3) Выделите интересующий регион на карте. Весь отмеченый регион должно быть хорошо видно\n"
             f"4) Наcтройте необходимые фильтры. Квартир в одном регионе с настроенными фильтрами не должно быть более 960, иначе регион не получится обработать."
             f" Поддерживается любой тип сделки: Покупка, Аренда, Посуточно\n"
             f"5) Отправьте ссылку на регион с настроенными фильтрами\n", reply_markup=markup
    )
    return ON_BEGIN_REGION_CREATION_EVENT


async def first_step_region_creation(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    url, error = validate_region_url(update.message.text, update.message.from_user.id)
    if error:
        await update.message.reply_text(error)
        return ON_BEGIN_REGION_CREATION_EVENT
    context.chat_data["creating_region"] = {"url": url}

    await update.message.reply_text(
        "Теперь отправьте название региона\n"
        "(Без пробелов и спецсимвов, но можно использовать нижнее подчеркивание '_')"
    )

    return FINAL_STEP_REGION_CREATION


async def final_step_region_creation(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    region_name, error = validate_region_name(update.message.text, update.message.from_user.id)
    if error:
        await update.message.reply_text(error)
        return FINAL_STEP_REGION_CREATION

    ParsingRegion.create(tg_user_id=update.message.from_user.id, url=context.chat_data["creating_region"]['url'],
                         name=region_name, active=True, platform='avito')

    del context.chat_data["creating_region"]
    await main_menu(update, context, send_new=True)
    srch_active = check_user_active_schedules(update.effective_user.id)
    txt = (f"Регион #{region_name} создан и включен. Поиск уже запущен, перезапускать его не обязательно\n\n"
           f"<i>* Кстати, регионы можно удалить или отредактировать в списке регионов, нажав на соотвествующий регион</i>") if srch_active else \
        (
            f"Регион #{region_name} создан. Запустите поиск что бы начать получать квартиры из всех созданных регионов\n\n"
            f"<i>* Кстати, регионы можно удалить или отредактировать в списке регионов, нажав на соотвествующий регион</i>")
    await update.message.reply_text(
        txt, parse_mode=ParseMode.HTML
    )
    return ON_MAIN_MENU_ACTIONS


async def changing(update: Update, context: ContextTypes.DEFAULT_TYPE):
    attr, region_id = update.callback_query.data.split(':')[1:]
    context.chat_data["current_changing"] = dict(id=region_id, attr=attr)
    markup = InlineKeyboardMarkup([[InlineKeyboardButton("Отмена", callback_data=f"region:{region_id}")]])
    await update.callback_query.edit_message_text(text=RUSSIFY_CHANGE.get(attr), reply_markup=markup)
    return CHANGE_SENT


async def change_sent(update: Update, context: ContextTypes.DEFAULT_TYPE):
    current_changing_key = context.chat_data["current_changing"]["attr"]
    current_changing_region_id = context.chat_data["current_changing"]["id"]
    current_changing_value = update.message.text
    validator = validators_map.get(current_changing_key)
    region_value, error = validator(current_changing_value, update.message.from_user.id)
    if error:
        await update.message.reply_text(error)
        return CHANGE_SENT

    region: ParsingRegion = ParsingRegion.get(ParsingRegion.id == current_changing_region_id)
    region_name = region.name
    setattr(region, current_changing_key, region_value)
    if current_changing_key == "url":
        region.map_position = None
        region.polygonX = None
        region.polygonY = None
        reset_sended_aparts(region.id, update.message.from_user.id)
    region.save()
    await region_info(update, context)
    srch_active = check_user_active_schedules(update.effective_user.id)
    txt = f"{get_russify_change(region_name, region.name, current_changing_key)}. Изменения применены автоматически. Поиск уже запущен, перезапускать его не обязательно" if srch_active else \
        f"{get_russify_change(region_name, region.name, current_changing_key)}. Изменения применены автоматически. Запустите поиск что бы начать получать квартиры из всех созданных регионов"
    await update.message.reply_text(
        txt
    )
    return ON_REGION_INFO_ACTION


async def delete_region(update: Update, context: ContextTypes.DEFAULT_TYPE):
    region_id = update.callback_query.data.split(':')[1]

    region = ParsingRegion.get(ParsingRegion.id == region_id)
    region.delete_instance()
    await update.callback_query.answer()
    await regions_list(update, context)
    srch_active = check_user_active_schedules(update.effective_user.id)
    txt = f"Регион #{region.name} удален и выключен. Поиск уже запущен, перезапускать его не обязательно" if srch_active else \
        f"Регион #{region.name} удален. Запустите поиск что бы начать получать квартиры из всех созданных регионов"
    await update.effective_user.send_message(
        txt, parse_mode=ParseMode.HTML)
    return ON_REGIONS_LIST_ACTIONS


async def done(update: Update, context: ContextTypes.DEFAULT_TYPE) -> int:
    """Display the gathered info and end the conversation."""
    if "choice" in context.user_data:
        del context.user_data["choice"]

    await update.message.reply_text(
        f"I learned these facts about you: Until next time!",
        reply_markup=ReplyKeyboardRemove(),
    )
    return ConversationHandler.END


async def change_region_active(update: Update, context: ContextTypes.DEFAULT_TYPE):
    region_id = update.callback_query.data.split(':')[1]

    region = ParsingRegion.get(ParsingRegion.id == region_id)
    region.active = not region.active
    region.save()
    return await region_info(update, context)


async def start_parsing(update: Update, context: ContextTypes.DEFAULT_TYPE):
    # if not region["active"]: continue
    # from celery.schedules import schedule
    # interval = schedule(run_every=15)
    await update.callback_query.answer()
    if check_user_active_schedules(update.effective_user.id):
        await application.bot.send_message(update.effective_user.id,
                                           "<strong>Ошибка запуска</strong>: Попробуйте подождать пару секунд перед запуском поиска, либо вызовите новое меню",
                                           parse_mode=ParseMode.HTML
                                           )
        return ON_MAIN_MENU_ACTIONS
    user = User.get(tg_user_id=update.effective_user.id)
    subscribe_alive, subscribe_time_left = is_subscribe_alive(user)
    if not subscribe_alive:
        await application.bot.send_message(update.effective_user.id,
                                           "<strong>Ошибка запуска</strong>: Ваша подписка закончилась, поиск квартир не запущен\n\n"
                                           f"Для возобновления поиска купите подписку и запустите поиск снова",
                                           parse_mode=ParseMode.HTML
                                           )
        return ON_MAIN_MENU_ACTIONS
    regions, err = validate_regions_to_run(update.effective_user.id)
    if err and not regions:
        return await request_empty_region_creation(update, context, mess=err+"\n\nСоздать новый регион?")
    run_parsing_entry(update.effective_user.id, (admin_chat_id,), celery_parser)
    await application.bot.send_message(update.effective_user.id,
                                       "Начинаю просматривать квартиры:)\n\nЧерез некоторое время (от 30 минут до нескольких часов) начнут приходить квартиры, начиная с наилучших вариантов по всем активным добавленным регионам."
                                       " Буду регулярно проверять и присылать новые отличные варианты\n\n"
                                       "<i>* Кстати, ИИ бывает ошибается, он не специально:) Можете сообщить о таких случаях в <a href='https://t.me/RealtyRenterBotSupport'>поддержку</a>, будем становиться лучше!</i>"
                                       , disable_web_page_preview=True, parse_mode=ParseMode.HTML
                                       )

    return await main_menu(update, context)


async def stop_parsing(update: Update, context: ContextTypes.DEFAULT_TYPE):
    run_stop_parsing_proc(update.effective_user.id)
    await update.callback_query.answer()
    await application.bot.send_message(update.effective_chat.id,
                                       "Поиск квартир отключен, подождите пару секунд перед повторным включением",
                                       **send_args)
    return await main_menu(update, context, parsing_stopped=True)


async def begin_start_parsing_admin(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.callback_query.answer()
    await update.callback_query.edit_message_text(
        text="Введите id пользователя которому нужно запустить поиск"
    )
    return ON_BEGIN_START_PARSING_ADMIN


async def start_parsing_admin(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user_id = int(update.message.text)
    if check_user_active_schedules(user_id):
        await application.bot.send_message(admin_chat_id,
                                           f"Поиск пользователя {user_id} уже запущен"
                                           , disable_web_page_preview=True, parse_mode=ParseMode.HTML
                                           )
    user = User.get(tg_user_id=user_id)
    subscribe_alive, subscribe_time_left = is_subscribe_alive(user)
    if not subscribe_alive:
        await application.bot.send_message(user_id,
                                           "<strong>Ошибка запуска</strong>: Ваша подписка закончилась, поиск квартир не запущен\n\n"
                                           f"Для возобновления поиска купите подписку и запустите поиск снова",
                                           parse_mode=ParseMode.HTML
                                           )
        return ON_MAIN_MENU_ACTIONS
    regions, err = validate_regions_to_run(user_id)
    if err and not regions:
        return await request_empty_region_creation(update, context, mess=err+"\n\nСоздать новый регион?")
    run_parsing_entry(user_id, (admin_chat_id,), celery_parser, admin_call=True)
    await application.bot.send_message(admin_chat_id,
                                       f"Поиск для пользователя {user_id} запущен снова"
                                       , disable_web_page_preview=True, parse_mode=ParseMode.HTML
                                       )

    return await main_menu(update, context)


async def begin_stop_parsing_admin(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.callback_query.answer()
    await update.callback_query.edit_message_text(
        text="Введите id пользователя которому нужно выключить поиск"
    )
    return ON_BEGIN_STOP_PARSING_ADMIN


async def stop_parsing_admin(update: Update, context: ContextTypes.DEFAULT_TYPE):
    user_id = int(update.message.text)
    run_stop_parsing_proc(user_id)
    await application.bot.send_message(admin_chat_id,
                                       f"Поиск для пользователя {user_id} отключен",
                                       **send_args)
    return await main_menu(update, context)


conv_handler = ConversationHandler(
    entry_points=[CommandHandler("start", main_menu),
                  CommandHandler("menu", main_menu)],
    states={
        CHANGE_SENT: [MessageHandler(
            filters.TEXT, change_sent
        ),
            CallbackQueryHandler(region_info, pattern=lambda c: c.startswith('region:'))],
        ON_REGION_INFO_ACTION: [CallbackQueryHandler(changing, pattern=lambda c: c.startswith('changing:')),
                                CallbackQueryHandler(regions_list, pattern="^region_info_back$"),
                                CallbackQueryHandler(delete_region, pattern=lambda c: c.startswith('delete:')),
                                CallbackQueryHandler(reset_region_sended_aparts,
                                                     pattern=lambda c: c.startswith('reset:')),
                                CallbackQueryHandler(main_menu, pattern="^main_menu$"),
                                CallbackQueryHandler(main_menu, pattern="^main_menu_apart$"),
                                CallbackQueryHandler(change_region_active,
                                                     pattern=lambda c: c.startswith('change_active:'))],
        ON_REGIONS_LIST_ACTIONS: [CallbackQueryHandler(region_info, pattern=lambda c: c.startswith('region:')),
                                  CallbackQueryHandler(main_menu, pattern="^regions_list_back$"),
                                  CallbackQueryHandler(begin_region_creation, pattern="^new_region_list$"),
                                  CallbackQueryHandler(begin_region_creation, pattern="^new_region_main$"),
                                  CallbackQueryHandler(main_menu, pattern="^main_menu_apart$")],
        ON_MAIN_MENU_ACTIONS: [
            CallbackQueryHandler(regions_list, pattern="^regions_list$"),
            CallbackQueryHandler(begin_region_creation, pattern="^new_region_main$"),
            CallbackQueryHandler(main_menu, pattern="^regions_list_back$"),
            CallbackQueryHandler(main_menu, pattern="^main_menu_apart$"),
            CallbackQueryHandler(get_sub_info, pattern="^get_sub$"),
            CallbackQueryHandler(start_parsing, pattern="^start_parsing$"),
            CallbackQueryHandler(begin_start_parsing_admin, pattern="^begin_start_parsing_admin$"),
            CallbackQueryHandler(begin_stop_parsing_admin, pattern="^begin_stop_parsing_admin$"),
            CallbackQueryHandler(stop_parsing, pattern="^stop_parsing$"),
            CallbackQueryHandler(check_payment, pattern=lambda c: c.startswith('check_payment:'))

        ],
        ON_BEGIN_REGION_CREATION_EVENT: [
            MessageHandler(
                filters.TEXT,
                first_step_region_creation,
            ),
            CallbackQueryHandler(main_menu, pattern="^cancel_creation_main$"),
            CallbackQueryHandler(regions_list, pattern="^cancel_creation_list$")
        ],
        FINAL_STEP_REGION_CREATION: [
            MessageHandler(
                filters.TEXT,
                final_step_region_creation,
            )],
        ON_GET_SUB_INFO_ACTION: [
            CallbackQueryHandler(get_payment_info, pattern=lambda c: c.startswith('pay_info:')),
            CallbackQueryHandler(main_menu, pattern="^main_menu$")
        ],
        ON_GET_PAYMENT_INFO_ACTION: [
            CallbackQueryHandler(get_sub_info, pattern=lambda c: c.startswith('back:')),
            CallbackQueryHandler(main_menu, pattern=lambda c: c.startswith('main_menu_payment:')),
            CallbackQueryHandler(check_payment, pattern=lambda c: c.startswith('check_payment:'))
        ],
        ON_BEGIN_START_PARSING_ADMIN: [
            MessageHandler(
                filters.TEXT,
                start_parsing_admin,
            )
        ],
        ON_BEGIN_STOP_PARSING_ADMIN: [
            MessageHandler(
                filters.TEXT,
                stop_parsing_admin,
            )
        ]

    },
    fallbacks=[MessageHandler(filters.Regex("^Done$"), done)],
    name="my_conversation",
    persistent=True,
    allow_reentry=True, block=False
)
