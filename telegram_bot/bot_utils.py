import threading
from datetime import datetime

from telegram import Update
from telegram.ext import ContextTypes
from redbeat import RedBeatSchedulerEntry
from redbeat.schedules import rrule
from celery_worker.utils import remove_tasks
from orm_models import StartedTasks, User, SendedFlat, DeniedFlat
from datetime import timedelta

args = dict(height=1080, width=1920, supports_streaming=True, caption="Обучающее видео")


async def send_tutorial(update: Update, context: ContextTypes.DEFAULT_TYPE):
    await update.message.reply_video(open('media/Renter_tutorial_desktop_14.mp4', 'rb'), **args)


def run_parsing_entry(user_id: int, excluded_ids: tuple[int], app, admin_call: bool = False):
    task_name = 'parse_flats'
    interval = 59
    schedule = rrule('MINUTELY', interval=interval)
    entry = RedBeatSchedulerEntry(f"periodic_parse_flats_{user_id}",
                                  task_name, schedule, kwargs={"user_id": user_id},
                                  app=app)
    try:

        task = StartedTasks.select().where((StartedTasks.tg_user_id == user_id) & (
                StartedTasks.task_name == task_name)).order_by(
            StartedTasks.task_started_datetime.desc()).get()
        diff = datetime.utcnow() - task.task_started_datetime
        if (diff.seconds // 60 >= interval) or (user_id in excluded_ids) or admin_call:
            entry.last_run_at = None

    except:
        entry.last_run_at = None
    entry.save()


def run_stop_parsing_proc(user_id: int):
    del_thread = threading.Thread(target=remove_tasks, args=(user_id,))
    del_thread.start()


def is_subscribe_alive(user: User) -> tuple[bool, timedelta]:
    print(user.subscribe_datetime_end)
    if user.subscribe_datetime_end is None:
        user.subscribe_datetime_end = datetime.utcnow() + timedelta(days=2, hours=3)
        print("herewq")
        user.save()
    subscribe_time_left = user.subscribe_datetime_end - datetime.utcnow()
    return subscribe_time_left.total_seconds() >= 0, subscribe_time_left


def reset_sended_aparts(region_id, tg_user_id):
    SendedFlat.delete().where(
        (SendedFlat.region_id == region_id) & (SendedFlat.tg_user_id == tg_user_id)).execute()
    DeniedFlat.delete().where(
        (DeniedFlat.region_id == region_id) & (DeniedFlat.tg_user_id == tg_user_id)).execute()
