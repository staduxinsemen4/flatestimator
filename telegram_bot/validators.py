from orm_models import db, ParsingRegion, User
from urllib.parse import urlsplit, urlparse, urlunparse
from typing import Tuple


def validate_region_url(url: str, user_id: int) -> Tuple[str, str]:
    if not url:
        return '', "В ответ нужно отправить сообщение текстом"
    parsed_url = urlparse(url)
    if not parsed_url.netloc:
        return '', "Неправильная ссылка"
    if "avito.ru" not in parsed_url.netloc:
        return '', "Ссылка на неподдерживаемый сайт"
    if "kvartiry/" not in parsed_url.path or "kuplyu" in parsed_url.path or "snimu" in parsed_url.path:
        return '', 'Ссылка должна вести на карту с квартирами'
    if "map=" not in parsed_url.query:
        return '', "Нужна ссылка карты с недвижимостью"
    if "drawId=" not in parsed_url.query:
        return '', "Нужно нарисовать регион на карте"
    if "m." in parsed_url.netloc:
        netloc = parsed_url.netloc.replace('m.', '')
        parsed_url = parsed_url._replace(netloc=netloc)
        url = str(urlunparse(parsed_url))
    if len(url) > 700:
        return '', f"Url региона слишком длинное"

    region = ParsingRegion.get_or_none(url=url, tg_user_id=user_id)
    if region:
        return '', f"Регион с такой ссылкой уже существует"
    return url, ''


def validate_region_name(name: str, user_id: int) -> Tuple[str, str]:
    if not name:
        return '', "В ответ нужно отправить сообщение текстом"
    name = ''.join(e for e in name if e.isalnum() or (e in ["_"]))
    if len(name) > 50:
        return '', f"Имя региона слишком длинное"

    region = ParsingRegion.get_or_none(name=name, tg_user_id=user_id)
    if region:
        return '', f"Регион с названием {name} уже существует"
    return name, ''


def validate_regions_to_run(user_id: int) -> tuple[list[ParsingRegion] | None, str]:
    regions = ParsingRegion.select().where(ParsingRegion.tg_user_id == user_id, ParsingRegion.active)
    if not regions:
        return None, ('Нет созданных или активных регионов\n\n'
                      'Поиск выключен, для включения поиска создайте или включите выключенные регионы в настройках и запустите поиск в главном меню')
    return regions, ''


def validate_regions_to_create(user_id: int) -> str:
    user: User = User.get(tg_user_id=user_id)
    regions = ParsingRegion.select().where(ParsingRegion.tg_user_id == user_id)
    if user_id == 1020614801:
        max_regions = 15
    elif user.is_subscriber:
        max_regions = 5
    else:
        max_regions = 1
    if len(regions) >= max_regions:
        region_limit_message = 'Достигнуто максимальное количество регионов для создания, удалите или отредактируйте уже существующие регионы'
        if not user.is_subscriber:
            region_limit_message += "\n\nПосле покупки подписки лимит регионов будет расширен до 5"
        return region_limit_message
    return ''


validators_map = dict(name=validate_region_name, url=validate_region_url)
