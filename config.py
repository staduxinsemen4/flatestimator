from os import environ as env
from dataclasses import dataclass
from dotenv import load_dotenv
from kombu import Queue
from yookassa import Configuration

load_dotenv()


@dataclass
class DataBaseConfig:
    host: str = env.get("POSTGRES_HOST")
    port: int = env.get("POSTGRES_PORT")
    database: str = env.get("POSTGRES_DATABASE")
    user: str = env.get("POSTGRES_USER")
    password: str = env.get("POSTGRES_PASSWORD")


class CeleryParserConfig:
    # default_retry_delay = 15
    # max_retries = 3
    # task_soft_time_limit = 2700
    task_time_limit = 2700
    # task_time_limit = 15
    # result_expires = 86400
    # result_persistent = True
    # result_extended = True
    # rate_limit = '1/m'
    redbeat_redis_url = "redis://redis:6379/0"
    beat_max_loop_interval = 10
    task_acks_late = True
    # task_acks_on_failure_or_timeout = False
    # task_reject_on_worker_lost = True
    # task_track_started = True
    broker_connection_retry_on_startup = True
    # redbeat_lock_timeout = 10
    task_queues = (
        Queue('default'),
        Queue('parser'),
    )
    task_routes = {
        'parse_flats': {'queue': 'parser'},
        'send_parsed_flats': {'queue': 'sender'},
    }


class CelerySenderConfig(CeleryParserConfig):
    task_queues = (
        Queue('sender'),
    )
    task_time_limit = 2000
    task_soft_time_limit = None


class CassaConfig:
    SHOP_ID = env.get("SHOP_ID")
    SHOP_SECRET_KEY = env.get("SHOP_SECRET_KEY")
    payment_builder = {
        "amount": {
            "value": "100",
            "currency": "RUB"
        },
        "confirmation": {
            "type": "redirect",
            "return_url": "https://t.me/RealtyRenterBot"
        },
        "capture": True,
        "receipt": {
            "customer": {
                "phone": "79000000000"
            },
            "items": [
                {
                    "description": "Покупка подписки на telegram бота Renter",
                    "quantity": "1",
                    "amount": {
                        "value": 1000,
                        "currency": "RUB"
                    },
                    "vat_code": "1",

                },
            ]
        },

    }


Configuration.configure(CassaConfig.SHOP_ID, CassaConfig.SHOP_SECRET_KEY)
